<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="MrFibunacci">
        <title>{block name=title}{/block} - MrFibunacci</title>
        {include "core/links.tpl"}
    </head>
    <body>
        <div id="wrapper">
            {include "core/navbar-top.tpl"}
            {include "core/navbar-static-side.tpl"}
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">{block name=pageName}{/block}</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="http://mrfibunacci.dev/">root</a></li>
                        {$breadcrumb}
                    </ol>
                    {block name=pageContent}{/block}
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        {include "core/scripts.tpl"}
    </body>
</html>
