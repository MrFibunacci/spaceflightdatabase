{extends file='main.tpl'}

{block name=title}Hangar-{$launchSysSpecs['Name']}{/block}
{block name=pageName}Hangar {/block}

{block name=pageContent}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                Configuration
            </div>
            <div class="panel panel-body">
                <table class="table display" id="SpaceFlightVehicleMissions" cellspacing="0" width="100%">
                    <thead>
                        <th>Version</th>
                        {foreach $launchSysSpecs['StageIDs'] as $key => $itt}
                        <th>Stage {$key+1}</th>
                        {/foreach}
                        {*<th>Strap-On</th>
                        <th>Stage 1</th>
                        <th>Stage 2</th>*}
                    </thead>
                    <tbody>
                    <tr>
                        <td>{$launchSysSpecs['Name']}</td>
                        {foreach $launchSysSpecs['StageIDs'] as $stage}
                        <td>{$stage}</td>
                        {/foreach}
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                Missions
            </div>
            <div class="panel panel-body">
                <div class="table-responsive">
                    <table class="table display" id="SpaceFlightVehicleMissions" cellspacing="0" width="100%">
                        <thead>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Outcome</th>
                        </thead>
                        <tbody>
                            {foreach $SpaceFlightVehicleTableData as $key => $dataset}
                                <tr>
                                    <td>{$key+1}</td>
                                    <td><a href='/sfdb/mission/{$dataset['ID']}'>{$dataset['Name']}</a></td>
                                    <td>{$dataset['StartDateTime']}</td>
                                    <td>{$dataset['outcome']}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#SpaceFlightVehicleMissions').DataTable();
    });
</script>
{/block}