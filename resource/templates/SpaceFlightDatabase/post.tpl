<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse{$ID}">{$postName}</a>
            </h4>
        </div>
        <div id="collapse{$ID}" class="panel-collapse collapse">
            <div class="panel-body">
                {foreach $spacemans as $spaceman}
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel panel-heading">
                                <b>Spaceman</b>
                            </div>
                            <div class="panel panel-body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td><a href="/sfdb/spaceman/{$spaceman['ID']}">{$spaceman['Name']}</a></td>
                                    </tr>
                                    <tr>
                                        <td>Birth</td>
                                        <td>{$spaceman['Birth']}</td>
                                    </tr>
                                    <tr>
                                        <td>Death</td>
                                        <td>{$spaceman['Death']}</td>
                                    </tr>
                                    <tr>
                                        <td>Nationality</td>
                                        <td>{$spaceman['Nationality']}</td>
                                    </tr>
                                    <tr>
                                        <td>EVAs</td>
                                        <td>{$spaceman['EVAs']}</td>
                                    </tr>
                                    <tr>
                                        <td>Job</td>
                                        <td>{$spaceman['Job']}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>{$spaceman['Status']}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>