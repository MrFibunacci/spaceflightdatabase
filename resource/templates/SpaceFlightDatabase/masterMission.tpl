{extends file='main.tpl'}

{block name=title}SpaceFlightDB Master Missions{/block}
{block name=pageName}Master Mission{/block}

{block name=pageContent}
<h1>{$missionName}</h1>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h1 class="panel-title">Missions</h1>
        </div>
        <div class="panel panel-body">
            <div class="table-responsive">
                <table class="table display" id="SpaceFlightMasterMissions" cellspacing="0" width="100%">
                    <thead>
                    {if $isSpecific}
                        <th>Name</th>
                        <th>Start Date</th>
                    {else}
                        <th>ID</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>All</th>
                        <th>Success</th>
                        <th>Failed</th>
                        <th>No Data</th>
                    {/if}
                    </thead>
                    <tbody>
                        {foreach $masterMissionData as $dataset}
                            <tr>
                            {if $isSpecific}
                                <td><a href="/sfdb/mission/{$dataset['ID']}">{$dataset['Name']}</a></td>
                                <td>{$dataset['StartDateTime']}</td>
                            {else}
                                <td>{$dataset['ID']}</td>
                                <td><a href="/sfdb/missions/master/{$dataset['ID']}">{$dataset['Name']}</a></td>
                                <td>{$dataset['StartDate']}</td>
                                <td>{$dataset['outcomeStats']['all']}</td>
                                <td>{$dataset['outcomeStats']['successful']}</td>
                                <td>{$dataset['outcomeStats']['failed']}</td>
                                <td>{$dataset['outcomeStats']['noData']}</td>
                            {/if}
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#SpaceFlightMasterMissions').DataTable();
    });
</script>
{/block}