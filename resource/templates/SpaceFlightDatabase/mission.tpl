{extends file='main.tpl'}

{block name=title}Mission {$Name}{/block}
{block name=pageName}Mission{/block}

{block name=pageContent}
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Mission</b>
            </div>
            <div class="panel panel-body">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Start</td>
                        <td>{$MissionMainInfo['StartDateTime']}</td>
                    </tr>
                    <tr>
                        <td>End</td>
                        <td>{$MissionMainInfo['EndDateTime']}</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>{$MissionMainInfo['Name']}</td>
                    </tr>
                    <tr>
                        <td>Agency</td>
                        <td>{$MissionMainInfo['byAgency']}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{$MissionMainInfo['Description']}</td>
                    </tr>
                    <tr>
                        <td>Outcome</td>
                        <td>{$MissionMainInfo['outcome']}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-warning">
            <div class="panel panel-heading">
                <b>Master Mission</b>
            </div>
            <div class="panel panel-body">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td><a href="/sfdb/missions/master/{$MasterMission['ID']}">{$MasterMission['Name']}</a></td>
                    </tr>
                    <tr>
                        <td>Start</td>
                        <td>{$MasterMission['StartDate']}</td>
                    </tr>
                    <tr>
                        <td>End</td>
                        <td>{$MasterMission['EndDate']}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{$payloadAndTrajectory}
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Launch Site</b>
            </div>
            <div class="panel panel-body">
                {if !isset($LaunchSiteData)}
                    <p class="text-danger">No data available</p>
                    <p>Do you know what data here should be? Contact me!</p>
                {else}
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>Name</td>
                            <td>{$LaunchSiteData['Name']}</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>{$LaunchSiteData['Location']}</td>
                        </tr>
                        <tr>
                            <td>Coordinates</td>
                            <td>{$LaunchSiteData['Coordinates']}</td>
                        </tr>
                        <tr>
                            <td>Built in</td>
                            <td>{$LaunchSiteData['BuiltIn']}</td>
                        </tr>
                        </tbody>
                    </table>
                {/if}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Space Launch System</b>
            </div>
            <div class="panel panel-body">
                {if !isset($SpaceLaunchSystem['Name']) and ($SpaceLaunchSystem['MaxPayloadLEO'] == "0,00 kg") and ($SpaceLaunchSystem['MaxHeight'] == "0,00 m") and ($SpaceLaunchSystem['MaxDiameter'] == "0,00 m")}
                    <p class="text-danger">No data available.</p>
                    <p>Do you know what data here should be? Contact me!</p>
                {else}
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td><a href="/sfdb/hangar/{$SpaceLaunchSystem['ID']}">{$SpaceLaunchSystem['Name']}</a></td>
                    </tr>
                    <tr>
                        <td>Max Payload to LEO</td>
                        <td>{$SpaceLaunchSystem['MaxPayloadLEO']}</td>
                    </tr>
                    <tr>
                        <td>Max Height</td>
                        <td>{$SpaceLaunchSystem['MaxHeight']}</td>
                    </tr>
                    <tr>
                        <td>Max Diameter</td>
                        <td>{$SpaceLaunchSystem['MaxDiameter']}</td>
                    </tr>
                    </tbody>
                </table>
                {/if}
            </div>
        </div>
    </div>
</div>
<div class="row">{$spaceman}</div>
{/block}