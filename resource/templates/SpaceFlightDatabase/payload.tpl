<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel panel-heading">
                <b>Payload</b>
            </div>
            <div class="panel panel-body">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{$payloadData['Name']}</td>
                    </tr>
                    <tr>
                        <td>Mission</td>
                        <td>{$payloadData['Mission']}</td>
                    </tr>
                    <tr>
                        <td>NSSDCA ID</td>
                        <td>{$payloadData['NSSDCA ID']}</td>
                    </tr>
                    <tr>
                        <td>SATCAT No.</td>
                        <td>{$payloadData['SATCATNo']}</td>
                    </tr>
                    <tr>
                        <td>Mass</td>
                        <td>{$payloadData['Mass']}</td>
                    </tr>
                    <tr>
                        <td>Alternate Names</td>
                        <td>{$payloadData['AlternateNames']}</td>
                    </tr>
                    <tr>
                        <td>Operator</td>
                        <td>{$payloadData['Operator']}</td>
                    </tr>
                    <tr>
                        <td>Contractor</td>
                        <td>{$payloadData['Contractor']}</td>
                    </tr>
                    <tr>
                        <td>Nation</td>
                        <td>{$payloadData['Nation'][0]}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>{$payloadData['Status'][0]}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" href="#collapse{$payloadData['ID']}">Little more Info</a>
                                    </div>
                                    <div id="collapse{$payloadData['ID']}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>Equipment</td>
                                                    <td>{$payloadData['Equipment'][0]}</td>
                                                </tr>
                                                <tr>
                                                    <td>Propulsion</td>
                                                    <td>{$payloadData['Propulsion'][0]}</td>
                                                </tr>
                                                <tr>
                                                    <td>Power</td>
                                                    <td>{$payloadData['Power'][0]}</td>
                                                </tr>
                                                <tr>
                                                    <td>Lifetime</td>
                                                    <td>{$payloadData['Lifetime']}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-body">
            {$missionEvents}
        </div>
    </div>
</div>
