{extends file='main.tpl'}

{block name=title}Spaceman {$name}{/block}
{block name=pageName}Spaceman {$name}{/block}

{block name=pageContent}
<div class="row">
	<div class="col-md-5 col-md-offset-4">
		<div class="panel panel-primary">

			<div class="panel panel-heading">
				<b class="panel-title">Spaceman</b>
			</div>
			<div class="panel panel-body">
				<table class="table table-striped">
					<tbody>
					<tr>
						<td>Name</td>
						<td>{$name}</td>
					</tr>
					<tr>
						<td>Birth</td>
						<td>{$birth}</td>
					</tr>
					<tr>
						<td>Death</td>
						<td>{$death}</td>
					</tr>
					<tr>
						<td>Nationality</td>
						<td>{$nationality}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>{$status}</td>
					</tr>
					<!--<tr>
						<td>Total Duration Of Spaceflight</td>
						<td></td>
					</tr>-->
					<tr>
						<td>EVAs</td>
						<td>{$eva}</td>
					</tr>
					<tr>
						<td>Training</td>
						<td>{$training}</td>
					</tr>
					<tr>
						<td>Job</td>
						<td>{$job}</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-md-offset-1 col-xs-12">
		<div class="panel panel-default">
			<div class="panel panel-heading">
				<h1 class="panel-title">Missions</h1>
			</div>
			<div class="panel panel-body">
				<ul class="timeline">
                    {foreach $missions as $mission}
					<li>
						<div class="timeline-badge success">{$mission['year']}</div>
						<div class="timeline-panel">
							<div class="timeline-heading">
								<h4 class="timeline-title"><a href="/sfdb/mission/{$mission['ID']}">{$mission['Name']}</a> - {$mission['StartDate']}</h4>
								<p><small class="text-muted"><i class="fa fa-clock-o"></i> {$mission['timeAgo']}</small></p>
							</div>
						</div>
					</li>
					{/foreach}
				</ul>
			</div>
		</div>
	</div>
</div>
{/block}