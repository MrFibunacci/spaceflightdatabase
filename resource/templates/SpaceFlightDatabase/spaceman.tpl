<div class="col-md-4">
    <div class="panel panel-primary">
        <div class="panel panel-heading">
            <b>Spaceman</b>
        </div>
        <div class="panel panel-body">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{$name}</td>
                </tr>
                <tr>
                    <td>Birth</td>
                    <td>{$birth}</td>
                </tr>
                <tr>
                    <td>Death</td>
                    <td>{$death}</td>
                </tr>
                <tr>
                    <td>Nationality</td>
                    <td>{$nationality}</td>
                </tr>
                <tr>
                    <td>EVAs</td>
                    <td>{$eva}</td>
                </tr>
                <tr>
                    <td>Training</td>
                    <td>{$training}</td>
                </tr>
                <tr>
                    <td>Job</td>
                    <td>{$job}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>