{extends file='main.tpl'}

{block name=title}SpaceFlightDB Master Missions{/block}
{block name=pageName}Master Mission{/block}

{block name=pageContent}
<div class="well">
    <h3>Oh oh!</h3>
    <p>It seems like you have no Mission specified. But that is no problem if you know what you are searching for.</p>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h1 class="panel-title">
                    How to find you are looking for.
                </h1>
            </div>
            <div class="panel panel-body">
                <ul>
                    <li><p>If you know the ID of the mission its easy, just add <code>/</code> and the ID to the URL and you are good.</p></li>
                    <li><p>If you know the <abr title="example: the main name of the missions like Explorer, for the explorer missions...">Master Mission</abr> you can  just search <a href="/sfdb/missions/master">here</a></p></li>
                    <li><p>Or just search in the table below.</p></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h1 class="panel-title">Mission starts per year</h1>
            </div>
            <div class="panel panel-body">
                <div id="statsByYear"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h1 class="panel-title">Missions</h1>
            </div>
            <div class="panel panel-body">
                <div class="table-responsive">
                    <table class="table display" id="SpaceFlightMissions" cellspacing="0" width="100%">
                        <thead>
                            <th>Name</th>
                            <th>StartDateTime</th>
                            <th>SpaceLaunchSystem</th>
                            <th>MasterMission</th>
                            <th>Manned</th>
                            <th>byAgency</th>
                            <th>outcome</th>
                        </thead>
                        <tbody>
                            {foreach $SpaceFlightMissionTableData as $dataset}
                                <tr>
                                    <td><a href='/sfdb/mission/{$dataset['ID']}'>{$dataset['Name']}</td>
                                    <td>{$dataset["StartDateTime"]}</td>
                                    <td><a href='/sfdb/hangar/{$dataset['SpaceLaunchSystemID']['ID']}'>{$dataset['SpaceLaunchSystemID']['Name']}</a></td>
                                    <td><a href='/sfdb/missions/master/{$dataset['MasterMissionIDs']['ID']}'>{$dataset['MasterMissionIDs']['Name']}</a></td>

                                    {if $dataset["AstronautIDs"] != null}
                                        <td>yes</td>
                                    {else}
                                        <td></td>
                                    {/if}

                                    <td>{$dataset["byAgency"]}</td>
                                    <td>{$dataset["outcome"]}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'statsByYear',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: {$dataForStats},
        // The name of the data record attribute that contains x-values.
        xkey: 'year',
        // A list of names of data record attributes that contain y-values.
        ykeys: ['value'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Value']
    });

    $(document).ready(function(){
        $('#SpaceFlightMissions').DataTable();
    });
</script>
{/block}