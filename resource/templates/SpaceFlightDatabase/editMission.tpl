<h1><p class="text-danger text-center">THIS IS AN TEMPLATE</p></h1>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Mission</b>
            </div>
            <div class="panel panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="StartDateTime">Start:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="StartDateTime" placeholder="format: yyyy-MM-dd hh:mm:ss" name="StartDateTime">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="EndDateTime">End:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="EndDateTime" placeholder="format: yyyy-MM-dd hh:mm:ss" name="EndDateTime">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Name">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="Name" placeholder="Mission name " name="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Agency">Agency:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="Agency" placeholder="The Agency" name="Agency">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Description">Description:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="Description" placeholder="Description" name="missionDescription"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Outcome">Outcome:</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="Outcome" name="Outcome">
                                <option>Successful</option>
                                <option>Failure</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-warning">
            <div class="panel panel-heading">
                <b>Master Mission</b>
            </div>
            <div class="panel panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="StartDateTime">Select:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="Outcome" name="Outcome">
                                <option>Sputnik</option>
                                <option>Vega</option>
                                <option>Explorer</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{$payloadAndTrajectory}
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Launch Site</b>
            </div>
            <div class="panel panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="LaunchSite">Select:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="LaunchSite" name="LaunchSite">
                                <option>Cape canev something</option>
                                <option>Baikonure vodka </option>
                                <option>I give up</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Space Launch System</b>
            </div>
            <div class="panel panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="StartDateTime">Select:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="Outcome" name="Outcome">
                                <option>Cape canev something</option>
                                <option>Baikonure vodka </option>
                                <option>I give up</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">{$spaceman}</div>
