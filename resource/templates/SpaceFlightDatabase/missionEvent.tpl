<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel panel-heading">
                <b>Mission Event</b>
        </div>
        <div class="panel panel-body">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td>Type</td>
                    <td>{$missionEvent['Type']}</td>
                </tr>
                <tr>
                    <td>Central Body</td>
                    <td>{$missionEvent['Central Body']}</td>
                </tr>
                <tr>
                    <td>At Mission Time</td>
                    <td>{$missionEvent['atMissionTime']}</td>
                </tr>
                <tr>
                    <td>Epoch Start (UTC)</td>
                    <td>{$missionEvent['EpochStart']}</td>
                </tr>
                <tr>
                    <td>Epoch Stop (UTC)</td>
                    <td>{$missionEvent['EpochStop']}</td>
                </tr>
                {if $missionEvent['Type'] == "Orbiter" or $missionEvent['Type'] == "Suborbital"}
                    <tr>
                        <td>Periapsis</td>
                        <td>{$missionEvent['Periapsis']}</td>
                    </tr>
                    <tr>
                        <td>Apoapsis</td>
                        <td>{$missionEvent['Apoapsis']}</td>
                    </tr>
                    <tr>
                        <td>Period</td>
                        <td>{$missionEvent['Period']}</td>
                    </tr>
                    <tr>
                        <td>Inclination</td>
                        <td>{$missionEvent['Inclination']}</td>
                    </tr>
                    <tr>
                        <td>Eccentricity</td>
                        <td>{$missionEvent['Eccentricity']}</td>
                    </tr>
                {elseif $missionEvent['Type'] == "Lander" or $missionEvent['Type'] == "Splashdown"}
                    <tr>
                        <td>Latitude</td>
                        <td>{$missionEvent['Latitude']}</td>
                    </tr>
                    <tr>
                        <td>Longitude</td>
                        <td>{$missionEvent['Longitude']}</td>
                    </tr>
                {elseif $missionEvent['Type'] == "Undock" or $missionEvent['Type'] == "Docking" or $missionEvent['Type'] == "Failed Docking" or $missionEvent['Type'] == "Separation"}
                    <tr>
                        <td>From Spacecraft</td>
                        <td><a href="/sfdb/payload/{$missionEvent['FromSpacecraftID']}">{$missionEvent['FromSpacecraftName']}</a></td>
                    </tr>
                {elseif $missionEvent['Type'] == "Flyby"}
                    <tr>
                        <td>Flyby Height</td>
                        <td>{$missionEvent['Apoapsis']}</td>
                    </tr>
                    <tr>
                        <td>Inclination</td>
                        <td>{$missionEvent['Inclination']}</td>
                    </tr>
                {/if}
                <tr>
                    <td>Description</td>
                    <td>{$missionEvent['Description']}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>