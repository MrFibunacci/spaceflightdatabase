{extends file='main.tpl'}

{block name=title}Space Flight Database{/block}
{block name=pageName}Space Flight Database{/block}

{block name=pageContent}
<div class="well">
    <h1>What?</h1>
    <p>What is this about? I recently search for anny possible way of getting detailed informations about space flight mission. But I had to get my information from manny, manny sources, like Wikipedia and some NASA, ESA, SapceX and more pages.</p>
    <p>So I thought why not make an own platform to create one source where you can every information about every Space mission you can think of.</p>

    <h2>Who am I?</h2>
    <p>Some guy from Germany, I call myself MrFibunacci, but there is nothing of interest about me.</p>

    <h2>Where?</h2>
    <p>Just look on the left side in the sub levels of "Space Flight Database".</p>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-rocket fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{$numOfPayloads}</div>
                        <div>Number of <b>Payloads</b> in Database!</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/payload">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-clipboard fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{$numOfMissions}</div>
                        <div>Number of <b>Missions</b> in Database!</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/mission">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-male fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{$numOfSpaceman}</div>
                        <div>Number of <b>Spaceman</b> in Database!</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/spaceman">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-archive fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">1</div>
                        <div>Number of Categories</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/sorted">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
{/block}