{extends file='main.tpl'}

{block name=title}Hangar{/block}
{block name=pageName}Hangar{/block}

{block name=pageContent}
<div class="row">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h1 class="panel-title">All Space Fight Vehicle in DB</h1>
        </div>
        <div class="panel panel-body">
            <div class="table-responsive">
                <table class="table display" id="SpaceFlightVehicle" cellspacing="0" width="100%">
                    <thead>
                    <th>#</th>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Total</th>
                        <th>Successful</th>
                        <th>Failed</th>
                        <th>no data set</th>
                    </thead>
                    <tbody>
                        {$SpaceFlightVehicleTableData}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-footer">
            <p class="text-warning">All numbers you see here are only from the database they can be different from the actual one. Caused by missing datasets.</p>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#SpaceFlightVehicle').DataTable();
    });
</script>
{/block}