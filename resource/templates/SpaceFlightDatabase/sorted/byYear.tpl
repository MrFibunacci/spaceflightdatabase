{extends file='main.tpl'}

{block name=title}Search list Entity's by Year{/block}
{block name=pageName}Sorted list Entity's by Year{/block}

{$year = 1956}

{block name=pageContent}
<div class="row">
    <div class="col-md-12">
        {if $dataSets === null}
        <div class="panel panel-default">
            <table class="table table-bordered">
                <tbody>
                {for $y = 0 to 5}
                    <tr>
                    {for $x = 0 to 10}
                        <td class="text-center"><a href="/sfdb/sorted/byYear/{$year}">{$year++}</a></td>
                    {/for}
                    </tr>
                {/for}
                </tbody>
            </table>
        </div>
        {else}
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Outcome</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $dataSets as $key => $dataSet}
                        <tr>
                            <td>{$key + 1}</td>
                            <td><a href="/sfdb/mission/{$dataSet['ID']}">{$dataSet['Name']}</a></td>
                            <td>{$dataSet['StartDateTime']}</td>
                            <td>{$dataSet['EndDateTime']}</td>
                            <td>{$dataSet['outcome']}</td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        {/if}
    </div>
</div>
{/block}