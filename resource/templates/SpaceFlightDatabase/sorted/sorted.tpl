{extends file='main.tpl'}


{block name=title}Search list Entity's sorted{/block}
{block name=pageName}Sorted list Entity's{/block}

{block name=pageContent}
<div class="well">
    <h3 class="text-center">Here you may find some category's in which I ordered the Database entity's</h3>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="list-group">
            <a href="/sfdb/sorted/byYear" class="list-group-item"><b>Ordered by Year</b></a>
            {*<a href="/sfdb/sorted/byNation" class="list-group-item">Ordered by Nation</a>*}
        </div>
    </div>
</div>
{/block}