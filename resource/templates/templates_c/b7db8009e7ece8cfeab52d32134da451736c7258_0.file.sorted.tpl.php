<?php
/* Smarty version 3.1.30, created on 2018-11-09 19:16:06
  from "/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/sorted/sorted.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5cee6c788e1_43020170',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7db8009e7ece8cfeab52d32134da451736c7258' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/sorted/sorted.tpl',
      1 => 1508330554,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5be5cee6c788e1_43020170 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11939334055be5cee6c66f11_62561966', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15572136465be5cee6c6e083_21417020', 'pageName');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6158172135be5cee6c75f50_96330613', 'pageContent');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'title'} */
class Block_11939334055be5cee6c66f11_62561966 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Search list Entity's sorted<?php
}
}
/* {/block 'title'} */
/* {block 'pageName'} */
class Block_15572136465be5cee6c6e083_21417020 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Sorted list Entity's<?php
}
}
/* {/block 'pageName'} */
/* {block 'pageContent'} */
class Block_6158172135be5cee6c75f50_96330613 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="well">
    <h3 class="text-center">Here you may find some category's in which I ordered the Database entity's</h3>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="list-group">
            <a href="/sfdb/sorted/byYear" class="list-group-item"><b>Ordered by Year</b></a>
            
        </div>
    </div>
</div>
<?php
}
}
/* {/block 'pageContent'} */
}
