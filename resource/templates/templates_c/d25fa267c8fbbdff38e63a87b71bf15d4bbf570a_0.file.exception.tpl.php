<?php
/* Smarty version 3.1.30, created on 2018-11-09 21:09:05
  from "/var/www/spaceflightdatabase/app/framework/Component/Exception/exception.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5e9611942e5_21654845',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd25fa267c8fbbdff38e63a87b71bf15d4bbf570a' => 
    array (
      0 => '/var/www/spaceflightdatabase/app/framework/Component/Exception/exception.tpl',
      1 => 1538670289,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5be5e9611942e5_21654845 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Oh sheet, something went wrong.</title>
</head>

<body>
    <header>
        <h1>Oh sheet, something went wrong.</h1>
    </header>
    <section>
        <p>Exception in <?php echo $_smarty_tpl->tpl_vars['filename']->value;?>
 at line <?php echo $_smarty_tpl->tpl_vars['lineNumber']->value;?>
</p>
        <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
        <p><pre><?php echo $_smarty_tpl->tpl_vars['trace']->value;?>
</pre></p>
    </section>
</body>
</html><?php }
}
