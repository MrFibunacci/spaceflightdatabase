<?php
/* Smarty version 3.1.30, created on 2018-11-09 21:17:04
  from "/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/hangar.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5eb40dde772_12661465',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'edab68a2d0e5fe67826e030595ac5a1490b766aa' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/hangar.tpl',
      1 => 1509031344,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5be5eb40dde772_12661465 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8547433075be5eb40dd9212_17449093', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21252669775be5eb40ddb524_34025857', 'pageName');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14532947235be5eb40dddbb6_23171236', 'pageContent');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'title'} */
class Block_8547433075be5eb40dd9212_17449093 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Hangar<?php
}
}
/* {/block 'title'} */
/* {block 'pageName'} */
class Block_21252669775be5eb40ddb524_34025857 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Hangar<?php
}
}
/* {/block 'pageName'} */
/* {block 'pageContent'} */
class Block_14532947235be5eb40dddbb6_23171236 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row">
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h1 class="panel-title">All Space Fight Vehicle in DB</h1>
        </div>
        <div class="panel panel-body">
            <div class="table-responsive">
                <table class="table display" id="SpaceFlightVehicle" cellspacing="0" width="100%">
                    <thead>
                    <th>#</th>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Total</th>
                        <th>Successful</th>
                        <th>Failed</th>
                        <th>no data set</th>
                    </thead>
                    <tbody>
                        <?php echo $_smarty_tpl->tpl_vars['SpaceFlightVehicleTableData']->value;?>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-footer">
            <p class="text-warning">All numbers you see here are only from the database they can be different from the actual one. Caused by missing datasets.</p>
        </div>
    </div>
</div>

<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('#SpaceFlightVehicle').DataTable();
    });
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'pageContent'} */
}
