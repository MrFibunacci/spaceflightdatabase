<?php
/* Smarty version 3.1.30, created on 2018-11-09 19:13:05
  from "/var/www/spaceflightdatabase/resource/templates/core/navbar-static-side.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5ce31e300a5_09816379',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '814e7621c74d60854dfd36a81acfb1fc3bcb6d4b' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/core/navbar-static-side.tpl',
      1 => 1541786557,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5be5ce31e300a5_09816379 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_counter')) require_once '/var/www/spaceflightdatabase/app/framework/Component/TemplateEngine/Smarty/plugins/function.counter.php';
?>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                

                
                <li>
                    <a href="/sfdb" tabindex="<?php echo smarty_function_counter(array('start'=>1,'skip'=>1),$_smarty_tpl);?>
">Main</a>
                </li>
                <li>
                    <a href="/sfdb/sorted" tabindex="<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
">Sorted</a>
                </li>
                <li>
                    <a href="/sfdb/mission" tabindex="<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
">Mission</a>
                </li>
                <li>
                    <a href="/sfdb/missions/master" tabindex="<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
">Master missions</a>
                </li>
                <li>
                    <a href="/sfdb/hangar" tabindex="<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
">Hangar</a>
                </li>
                <li>
                    <a href="/about" tabindex="<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
"><i class="fa fa-info"></i> About</a>
                </li>
                <li>
                    <a href="/changes" tabindex="<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
"><i class="fa fa-code-fork"></i> Chnages</a>
                </li>
                

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav><?php }
}
