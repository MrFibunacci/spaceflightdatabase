<?php
/* Smarty version 3.1.30, created on 2018-11-09 20:16:59
  from "/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/noMissionSpecified.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5eb3bccb298_35675056',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c56e55343216c4187babafad97518d67ba21d433' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/noMissionSpecified.tpl',
      1 => 1508149811,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5be5eb3bccb298_35675056 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8323143015be5eb3bcbb670_37399641', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7229803315be5eb3bcbd7d5_20942078', 'pageName');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9622571565be5eb3bcc9707_37008873', 'pageContent');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'title'} */
class Block_8323143015be5eb3bcbb670_37399641 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
SpaceFlightDB Master Missions<?php
}
}
/* {/block 'title'} */
/* {block 'pageName'} */
class Block_7229803315be5eb3bcbd7d5_20942078 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Master Mission<?php
}
}
/* {/block 'pageName'} */
/* {block 'pageContent'} */
class Block_9622571565be5eb3bcc9707_37008873 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="well">
    <h3>Oh oh!</h3>
    <p>It seems like you have no Mission specified. But that is no problem if you know what you are searching for.</p>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h1 class="panel-title">
                    How to find you are looking for.
                </h1>
            </div>
            <div class="panel panel-body">
                <ul>
                    <li><p>If you know the ID of the mission its easy, just add <code>/</code> and the ID to the URL and you are good.</p></li>
                    <li><p>If you know the <abr title="example: the main name of the missions like Explorer, for the explorer missions...">Master Mission</abr> you can  just search <a href="/sfdb/missions/master">here</a></p></li>
                    <li><p>Or just search in the table below.</p></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h1 class="panel-title">Mission starts per year</h1>
            </div>
            <div class="panel panel-body">
                <div id="statsByYear"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h1 class="panel-title">Missions</h1>
            </div>
            <div class="panel panel-body">
                <div class="table-responsive">
                    <table class="table display" id="SpaceFlightMissions" cellspacing="0" width="100%">
                        <thead>
                            <th>Name</th>
                            <th>StartDateTime</th>
                            <th>SpaceLaunchSystem</th>
                            <th>MasterMission</th>
                            <th>Manned</th>
                            <th>byAgency</th>
                            <th>outcome</th>
                        </thead>
                        <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['SpaceFlightMissionTableData']->value, 'dataset');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['dataset']->value) {
?>
                                <tr>
                                    <td><a href='/sfdb/mission/<?php echo $_smarty_tpl->tpl_vars['dataset']->value['ID'];?>
'><?php echo $_smarty_tpl->tpl_vars['dataset']->value['Name'];?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['dataset']->value["StartDateTime"];?>
</td>
                                    <td><a href='/sfdb/hangar/<?php echo $_smarty_tpl->tpl_vars['dataset']->value['SpaceLaunchSystemID']['ID'];?>
'><?php echo $_smarty_tpl->tpl_vars['dataset']->value['SpaceLaunchSystemID']['Name'];?>
</a></td>
                                    <td><a href='/sfdb/missions/master/<?php echo $_smarty_tpl->tpl_vars['dataset']->value['MasterMissionIDs']['ID'];?>
'><?php echo $_smarty_tpl->tpl_vars['dataset']->value['MasterMissionIDs']['Name'];?>
</a></td>

                                    <?php if ($_smarty_tpl->tpl_vars['dataset']->value["AstronautIDs"] != null) {?>
                                        <td>yes</td>
                                    <?php } else { ?>
                                        <td></td>
                                    <?php }?>

                                    <td><?php echo $_smarty_tpl->tpl_vars['dataset']->value["byAgency"];?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['dataset']->value["outcome"];?>
</td>
                                </tr>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'statsByYear',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: <?php echo $_smarty_tpl->tpl_vars['dataForStats']->value;?>
,
        // The name of the data record attribute that contains x-values.
        xkey: 'year',
        // A list of names of data record attributes that contain y-values.
        ykeys: ['value'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Value']
    });

    $(document).ready(function(){
        $('#SpaceFlightMissions').DataTable();
    });
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'pageContent'} */
}
