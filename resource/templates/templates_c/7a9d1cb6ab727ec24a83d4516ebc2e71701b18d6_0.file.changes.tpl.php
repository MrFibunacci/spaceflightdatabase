<?php
/* Smarty version 3.1.30, created on 2018-11-09 22:20:07
  from "/var/www/spaceflightdatabase/resource/templates/changes/changes.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5fa07c3eca5_86251811',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a9d1cb6ab727ec24a83d4516ebc2e71701b18d6' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/changes/changes.tpl',
      1 => 1541798334,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5be5fa07c3eca5_86251811 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="row">
    <div class="col-md-11">
        <div class="panel panel-default">
            <div class="panel panel-body">
                <h1>Version 0.1.1 <small>- 2018.11.09</small></h1>
                <div class="row">
                    <div class="col-md-1">
                        <h4><span class="label label-info">Changes: </span></h4>
                    </div>
                    <div class="col-md-11">
                        <h4> Updated to Framy version 0.3</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-11">
        <div class="panel panel-default">
            <div class="panel panel-body">
                <h1>Version 0.1.0 <small>- 2016.09.11</small></h1>
                <div class="row">
                    <div class="col-md-1">
                        <h4><span class="label label-info">Info: </span></h4>
                    </div>
                    <div class="col-md-11">
                        <h4> Initial Version</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php }
}
