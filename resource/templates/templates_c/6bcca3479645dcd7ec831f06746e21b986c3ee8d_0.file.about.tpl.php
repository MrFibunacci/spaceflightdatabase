<?php
/* Smarty version 3.1.30, created on 2018-11-09 21:25:06
  from "/var/www/spaceflightdatabase/resource/templates/about.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5ed22a719d3_39350721',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6bcca3479645dcd7ec831f06746e21b986c3ee8d' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/about.tpl',
      1 => 1541795104,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5be5ed22a719d3_39350721 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16276002565be5ed22a66617_58356849', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18708248565be5ed22a6ff28_72514372', 'pageName');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16889745995be5ed22a71394_83859926', 'pageContent');
?>

<?php $_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'title'} */
class Block_16276002565be5ed22a66617_58356849 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
About<?php
}
}
/* {/block 'title'} */
/* {block 'pageName'} */
class Block_18708248565be5ed22a6ff28_72514372 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
About<?php
}
}
/* {/block 'pageName'} */
/* {block 'pageContent'} */
class Block_16889745995be5ed22a71394_83859926 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    <h1 class="panel-title">How?!</h1>
                </div>
                <div class="panel panel-body">
                    <p><label class="fa fa-code"></label> with <label class="fa fa-heart"></label> and <a href="https://github.com/MrFibunacci/Framy">Framy</a> (v0.3) an self made <a href="http://php.net/manual/de/intro-whatis.php">PHP</a>  Framework, it's not the best but still good.</p>
                    <p>Not to forget I used <a href="http://getbootstrap.com/">Bootstrap</a> (v.3.3.7)</p>
                    <p>Under usage of the SB Admin v2.0 Theme</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}
}
/* {/block 'pageContent'} */
}
