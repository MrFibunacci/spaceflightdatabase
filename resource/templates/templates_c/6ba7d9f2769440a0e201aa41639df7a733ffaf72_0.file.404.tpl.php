<?php
/* Smarty version 3.1.30, created on 2018-11-09 19:13:12
  from "/var/www/spaceflightdatabase/resource/templates/core/error/404.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5ce382a3272_48851066',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6ba7d9f2769440a0e201aa41639df7a733ffaf72' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/core/error/404.tpl',
      1 => 1508149811,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5be5ce382a3272_48851066 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="MrFibunacci">

        <title>404 - not found</title>

        <!-- Bootstrap Core CSS -->
        <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <?php if ($_smarty_tpl->tpl_vars['gBrowserInfo']->value['browser'] == 'ie') {?>
            <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
        <?php }?>
    </head>
    <body>

        <div class="container">
            <!-- Jumbotron -->
            <div class="jumbotron">
                <h1><i class="fa fa-frown-o red"></i> 404 Not Found</h1>
                <p class="lead">We couldn't find what you're looking for on <em><span id="display-domain"></span></em>.</p>
                <p><a href="/" class="btn btn-default btn-lg"><span class="green">Take Me To The Homepage</span></a>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="body-content">
                <div class="row">
                    <div class="col-md-6">
                        <h2>What happened?</h2>
                        <p class="lead">A 404 error status implies that the file or page that you're looking for could not be found.</p>
                    </div>
                    <div class="col-md-6">
                        <h2>What can I do?</h2>
                        <p class="lead">If you're a site visitor</p>
                        <p>Please use your browser's back button and check that you're in the right place. If you need immediate assistance, please send us an email instead.</p>
                        <p class="lead">If you're the site owner</p>
                        <p>Please check that you're in the right place and get in touch with your website provider if you believe this to be an error.</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <?php echo '<script'; ?>
 src="vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>

        <!-- Bootstrap Core JavaScript -->
        <?php echo '<script'; ?>
 src="vendor/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

        <!-- Metis Menu Plugin JavaScript -->
        <?php echo '<script'; ?>
 src="vendor/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
>

        <!-- Custom Theme JavaScript -->
        <?php echo '<script'; ?>
 src="dist/js/sb-admin-2.js"><?php echo '</script'; ?>
>

    </body>
</html><?php }
}
