<?php
/* Smarty version 3.1.30, created on 2018-11-09 19:13:05
  from "/var/www/spaceflightdatabase/resource/templates/blank.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5ce31dedcf5_43517493',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0b25a5a4150163cf75787e99519696c82ffd4702' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/blank.tpl',
      1 => 1508149811,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:core/links.tpl' => 1,
    'file:core/navbar-top.tpl' => 1,
    'file:core/navbar-static-side.tpl' => 1,
    'file:core/scripts.tpl' => 1,
  ),
),false)) {
function content_5be5ce31dedcf5_43517493 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="MrFibunacci">
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 - MrFibunacci</title>
        <?php $_smarty_tpl->_subTemplateRender("file:core/links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </head>
    <body>
        <div id="wrapper">
            <?php $_smarty_tpl->_subTemplateRender("file:core/navbar-top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php $_smarty_tpl->_subTemplateRender("file:core/navbar-static-side.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?php echo $_smarty_tpl->tpl_vars['pageName']->value;?>
</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="http://mrfibunacci.dev/">root</a></li>
                        <?php echo $_smarty_tpl->tpl_vars['breadcrumb']->value;?>

                    </ol>
                    <?php echo $_smarty_tpl->tpl_vars['pageContent']->value;?>

                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <?php $_smarty_tpl->_subTemplateRender("file:core/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </body>
</html>
<?php }
}
