<?php
/* Smarty version 3.1.30, created on 2018-11-09 19:16:06
  from "/var/www/spaceflightdatabase/resource/templates/main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5cee6ca81f0_19057502',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '813779e5b585bc77f06010e99d459b6fc3218121' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/main.tpl',
      1 => 1526756088,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:core/links.tpl' => 1,
    'file:core/navbar-top.tpl' => 1,
    'file:core/navbar-static-side.tpl' => 1,
    'file:core/scripts.tpl' => 1,
  ),
),false)) {
function content_5be5cee6ca81f0_19057502 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="MrFibunacci">
        <title><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9055411405be5cee6c93bd9_67486890', 'title');
?>
 - MrFibunacci</title>
        <?php $_smarty_tpl->_subTemplateRender("file:core/links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </head>
    <body>
        <div id="wrapper">
            <?php $_smarty_tpl->_subTemplateRender("file:core/navbar-top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php $_smarty_tpl->_subTemplateRender("file:core/navbar-static-side.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10930930475be5cee6c9a294_51870424', 'pageName');
?>
</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="http://mrfibunacci.dev/">root</a></li>
                        <?php echo $_smarty_tpl->tpl_vars['breadcrumb']->value;?>

                    </ol>
                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13989810455be5cee6c9cd46_10495226', 'pageContent');
?>

                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <?php $_smarty_tpl->_subTemplateRender("file:core/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </body>
</html>
<?php }
/* {block 'title'} */
class Block_9055411405be5cee6c93bd9_67486890 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'title'} */
/* {block 'pageName'} */
class Block_10930930475be5cee6c9a294_51870424 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'pageName'} */
/* {block 'pageContent'} */
class Block_13989810455be5cee6c9cd46_10495226 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'pageContent'} */
}
