<?php
/* Smarty version 3.1.30, created on 2018-11-09 19:13:05
  from "/var/www/spaceflightdatabase/resource/templates/core/scripts.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5ce31e3c5e4_06129612',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '210e7fab3a860a89d990e2854e632ea5e60eb703' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/core/scripts.tpl',
      1 => 1508149811,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5be5ce31e3c5e4_06129612 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- jQuery -->
<?php echo '<script'; ?>
 src="/vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>

<!-- Bootstrap Core JavaScript -->
<?php echo '<script'; ?>
 src="/vendor/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

<!-- Metis Menu Plugin JavaScript -->
<?php echo '<script'; ?>
 src="/vendor/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
>

<!-- Custom Theme JavaScript -->
<?php echo '<script'; ?>
 src="/dist/js/sb-admin-2.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="/data/morris-data.js"><?php echo '</script'; ?>
>

<!-- DataTables JavaScript -->
<?php echo '<script'; ?>
 src="../vendor/datatables/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="../vendor/datatables-responsive/dataTables.responsive.js"><?php echo '</script'; ?>
>
<?php }
}
