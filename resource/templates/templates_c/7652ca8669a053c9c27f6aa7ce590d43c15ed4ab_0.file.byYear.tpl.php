<?php
/* Smarty version 3.1.30, created on 2018-11-09 21:16:42
  from "/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/sorted/byYear.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5eb2aec7dc0_97876050',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7652ca8669a053c9c27f6aa7ce590d43c15ed4ab' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/sorted/byYear.tpl',
      1 => 1508331187,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5be5eb2aec7dc0_97876050 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13428438075be5eb2ae54ef4_50077671', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9137455955be5eb2ae56ea9_39111054', 'pageName');
?>


<?php $_smarty_tpl->_assignInScope('year', 1956);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19565594125be5eb2aec1b93_65725187', 'pageContent');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'title'} */
class Block_13428438075be5eb2ae54ef4_50077671 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Search list Entity's by Year<?php
}
}
/* {/block 'title'} */
/* {block 'pageName'} */
class Block_9137455955be5eb2ae56ea9_39111054 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Sorted list Entity's by Year<?php
}
}
/* {/block 'pageName'} */
/* {block 'pageContent'} */
class Block_19565594125be5eb2aec1b93_65725187 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row">
    <div class="col-md-12">
        <?php if ($_smarty_tpl->tpl_vars['dataSets']->value === null) {?>
        <div class="panel panel-default">
            <table class="table table-bordered">
                <tbody>
                <?php
$_smarty_tpl->tpl_vars['y'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['y']->step = 1;$_smarty_tpl->tpl_vars['y']->total = (int) ceil(($_smarty_tpl->tpl_vars['y']->step > 0 ? 5+1 - (0) : 0-(5)+1)/abs($_smarty_tpl->tpl_vars['y']->step));
if ($_smarty_tpl->tpl_vars['y']->total > 0) {
for ($_smarty_tpl->tpl_vars['y']->value = 0, $_smarty_tpl->tpl_vars['y']->iteration = 1;$_smarty_tpl->tpl_vars['y']->iteration <= $_smarty_tpl->tpl_vars['y']->total;$_smarty_tpl->tpl_vars['y']->value += $_smarty_tpl->tpl_vars['y']->step, $_smarty_tpl->tpl_vars['y']->iteration++) {
$_smarty_tpl->tpl_vars['y']->first = $_smarty_tpl->tpl_vars['y']->iteration == 1;$_smarty_tpl->tpl_vars['y']->last = $_smarty_tpl->tpl_vars['y']->iteration == $_smarty_tpl->tpl_vars['y']->total;?>
                    <tr>
                    <?php
$_smarty_tpl->tpl_vars['x'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['x']->step = 1;$_smarty_tpl->tpl_vars['x']->total = (int) ceil(($_smarty_tpl->tpl_vars['x']->step > 0 ? 10+1 - (0) : 0-(10)+1)/abs($_smarty_tpl->tpl_vars['x']->step));
if ($_smarty_tpl->tpl_vars['x']->total > 0) {
for ($_smarty_tpl->tpl_vars['x']->value = 0, $_smarty_tpl->tpl_vars['x']->iteration = 1;$_smarty_tpl->tpl_vars['x']->iteration <= $_smarty_tpl->tpl_vars['x']->total;$_smarty_tpl->tpl_vars['x']->value += $_smarty_tpl->tpl_vars['x']->step, $_smarty_tpl->tpl_vars['x']->iteration++) {
$_smarty_tpl->tpl_vars['x']->first = $_smarty_tpl->tpl_vars['x']->iteration == 1;$_smarty_tpl->tpl_vars['x']->last = $_smarty_tpl->tpl_vars['x']->iteration == $_smarty_tpl->tpl_vars['x']->total;?>
                        <td class="text-center"><a href="/sfdb/sorted/byYear/<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['year']->value++;?>
</a></td>
                    <?php }
}
?>

                    </tr>
                <?php }
}
?>

                </tbody>
            </table>
        </div>
        <?php } else { ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Outcome</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dataSets']->value, 'dataSet', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['dataSet']->value) {
?>
                        <tr>
                            <td><?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
</td>
                            <td><a href="/sfdb/mission/<?php echo $_smarty_tpl->tpl_vars['dataSet']->value['ID'];?>
"><?php echo $_smarty_tpl->tpl_vars['dataSet']->value['Name'];?>
</a></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['dataSet']->value['StartDateTime'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['dataSet']->value['EndDateTime'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['dataSet']->value['outcome'];?>
</td>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </tbody>
            </table>
        <?php }?>
    </div>
</div>
<?php
}
}
/* {/block 'pageContent'} */
}
