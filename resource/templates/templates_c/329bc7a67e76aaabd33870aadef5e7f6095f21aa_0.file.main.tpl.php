<?php
/* Smarty version 3.1.30, created on 2018-11-09 21:13:51
  from "/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5be5ea7f27cc59_54247058',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '329bc7a67e76aaabd33870aadef5e7f6095f21aa' => 
    array (
      0 => '/var/www/spaceflightdatabase/resource/templates/SpaceFlightDatabase/main.tpl',
      1 => 1508330776,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5be5ea7f27cc59_54247058 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1379630475be5ea7f276b09_04566803', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1790612015be5ea7f278714_36423136', 'pageName');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6020321975be5ea7f27c093_84427847', 'pageContent');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'title'} */
class Block_1379630475be5ea7f276b09_04566803 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Space Flight Database<?php
}
}
/* {/block 'title'} */
/* {block 'pageName'} */
class Block_1790612015be5ea7f278714_36423136 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Space Flight Database<?php
}
}
/* {/block 'pageName'} */
/* {block 'pageContent'} */
class Block_6020321975be5ea7f27c093_84427847 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="well">
    <h1>What?</h1>
    <p>What is this about? I recently search for anny possible way of getting detailed informations about space flight mission. But I had to get my information from manny, manny sources, like Wikipedia and some NASA, ESA, SapceX and more pages.</p>
    <p>So I thought why not make an own platform to create one source where you can every information about every Space mission you can think of.</p>

    <h2>Who am I?</h2>
    <p>Some guy from Germany, I call myself MrFibunacci, but there is nothing of interest about me.</p>

    <h2>Where?</h2>
    <p>Just look on the left side in the sub levels of "Space Flight Database".</p>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-rocket fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo $_smarty_tpl->tpl_vars['numOfPayloads']->value;?>
</div>
                        <div>Number of <b>Payloads</b> in Database!</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/payload">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-clipboard fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo $_smarty_tpl->tpl_vars['numOfMissions']->value;?>
</div>
                        <div>Number of <b>Missions</b> in Database!</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/mission">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-male fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo $_smarty_tpl->tpl_vars['numOfSpaceman']->value;?>
</div>
                        <div>Number of <b>Spaceman</b> in Database!</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/spaceman">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-archive fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">1</div>
                        <div>Number of Categories</div>
                    </div>
                </div>
            </div>
            <a href="/sfdb/sorted">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
<?php
}
}
/* {/block 'pageContent'} */
}
