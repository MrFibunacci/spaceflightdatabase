{extends file='main.tpl'}

{block name=title}About{/block}
{block name=pageName}About{/block}

{block name=pageContent}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    <h1 class="panel-title">How?!</h1>
                </div>
                <div class="panel panel-body">
                    <p><label class="fa fa-code"></label> with <label class="fa fa-heart"></label> and <a href="https://github.com/MrFibunacci/Framy">Framy</a> (v0.3) an self made <a href="http://php.net/manual/de/intro-whatis.php">PHP</a>  Framework, it's not the best but still good.</p>
                    <p>Not to forget I used <a href="http://getbootstrap.com/">Bootstrap</a> (v.3.3.7)</p>
                    <p>Under usage of the SB Admin v2.0 Theme</p>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}
