<div class="row">
    <div class="col-md-11">
        <div class="panel panel-default">
            <div class="panel panel-body">
                <h1>Version 0.1.1 <small>- 2018.11.09</small></h1>
                <div class="row">
                    <div class="col-md-1">
                        <h4><span class="label label-info">Changes: </span></h4>
                    </div>
                    <div class="col-md-11">
                        <h4> Updated to Framy version 0.3</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-11">
        <div class="panel panel-default">
            <div class="panel panel-body">
                <h1>Version 0.1.0 <small>- 2016.09.11</small></h1>
                <div class="row">
                    <div class="col-md-1">
                        <h4><span class="label label-info">Info: </span></h4>
                    </div>
                    <div class="col-md-11">
                        <h4> Initial Version</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{*

Guiding Principles
    -Changelogs are for humans, not machines.
    -There should be an entry for every single version.
    -The same types of changes should be grouped.
    -Versions and sections should be linkable.
    -The latest version comes first.
    -The release date of each version is displayed.
    -Mention whether you follow Semantic Versioning.
Types of changes
    Added for new features.
    Changed for changes in existing functionality.
    Deprecated for soon-to-be removed features.
    Removed for now removed features.
    Fixed for any bug fixes.
    Security in case of vulnerabilities.

<div class="row">
    <div class="col-md-11">
        <div class="panel panel-default">
            <div class="panel panel-body">
                <h1>Version 0.0.1 <small>feddich am 11.09.2016</small></h1>
                <div class="row">
                    <div class="col-md-1">
                        <h4><span class="label label-info">Info: </span></h4>
                    </div>
                    <div class="col-md-11">
                        <h4> Total tolle Informationen die Informieren</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <h4><span class="label label-primary">Neu: </span></h4>
                    </div>
                    <div class="col-md-11">
                        <h4> Neue Neuerungen</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <h4><span class="label label-warning">Fix: </span></h4>
                    </div>
                    <div class="col-md-11">
                        <h4>Das wurde wieder ganz gemacht</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>*}
