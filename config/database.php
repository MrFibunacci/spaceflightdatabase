<?php

    return [

        /*
         |----------------------------------------
         | Database Connections
         |----------------------------------------
         |
         | Here you can configure your connections. You can ether use the
         | here shown syntax or the syntax from Medoo. The key of the array
         | in the example 'mysql' is the name of the connection.
         |
         */

        'connections' => [
            'SpaceFlightDBnew' => [
                'driver' => 'mysql',
                'host' => 'localhost',
                'port' => 3306,
                'database' => 'SpaceFlightNew',
                'username' => 'root',
                'password' => 'test',
                'charset' => 'utf8',
            ],
        ]
    ];