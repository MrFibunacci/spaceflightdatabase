<?php
    namespace app\custom\Http\Controller;

    use app\framework\Component\http\Model\Model;

    class Login extends Model
    {
        protected $connection = 'main';

        public function logMeIn()
        {
            $userData = $this->DB()->select(['ID', 'username', 'email', 'registered'], ['username' => $_POST['username'], 'password' => $_POST['password']], 'user');

            if(!$this->isNull($userData)) {
                $_SESSION['user'] = $userData;
            }
        }
    }