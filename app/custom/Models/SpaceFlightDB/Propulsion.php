<?php
    namespace app\custom\Models\SpaceFlightDB;


    use app\framework\Component\StdLib\SingletonTrait;

    class Propulsion extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Propulsion';
    }