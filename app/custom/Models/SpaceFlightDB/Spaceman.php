<?php

    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class Spaceman extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Spaceman';

        public function getByID($ID, $columns = '*')
        {
            $temp = parent::getByID($ID, $columns);

            $temp['Nationality'] = Nation::getInstance()->getByID($temp['Nationality'], 'Name');
            $temp['Status']      = Status::getInstance()->getByID($temp['Status'], 'Name');

            return $temp;
        }


    }