<?php
    namespace app\custom\Models\SpaceFlightDB;


    use app\framework\Component\StdLib\SingletonTrait;

    class Crew extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Crew';
    }