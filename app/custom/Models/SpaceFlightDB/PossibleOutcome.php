<?php
    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class PossibleOutcome extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'PossibleOutcome';
    }