<?php

    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class Missions extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Missions';

        public function getOutcomeByID($ID)
        {
            return $this->get('Name', ['ID' => $ID], 'PossibleOutcome');
        }
    }