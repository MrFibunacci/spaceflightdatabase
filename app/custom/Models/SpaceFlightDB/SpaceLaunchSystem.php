<?php
    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class SpaceLaunchSystem extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'SpaceLaunchSystem';
    }