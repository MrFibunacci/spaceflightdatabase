<?php
    namespace app\custom\Models\SpaceFlightDB;


    use app\framework\Component\StdLib\SingletonTrait;

    class MasterMissions extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'MasterMissions';
    }