<?php

    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class Status extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Status';
    }