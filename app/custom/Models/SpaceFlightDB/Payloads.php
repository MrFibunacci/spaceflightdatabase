<?php

    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class Payloads extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Payloads';

        public function getMissionEvents(&$payloadData)
        {
            return MissionEvents::getInstance()->getAllWhere('*', ['ID' => $payloadData['MissionEventIDs']]);
        }

        public function fullFillData(&$payloadData)
        {
            $payloadData['Equipment']  = Equipment::getInstance()->getAllWhere('Name', ['ID'=>explode(",", $payloadData['Equipment'])]);
            $payloadData['Propulsion'] = Propulsion::getInstance()->getAllWhere("Name", ['ID'=>explode(",", $payloadData['Propulsion'])]);
            $payloadData['Power']      = Power::getInstance()->getAllWhere("Name", ['ID'=>explode(",", $payloadData['Power'])]);
            $payloadData['Nation']     = Nation::getInstance()->getAllWhere("Name", ['ID'=>explode(",", $payloadData['Nation'])]);
            $payloadData['Status']     = Status::getInstance()->getAllWhere("Name", ['ID'=>explode(",", $payloadData['Status'])]);
        }


    }