<?php

    namespace app\custom\Models\SpaceFlightDB;


    use app\framework\Component\http\Model\Model;
    use app\framework\Component\StdLib\SingletonTrait;

    abstract class SpaceFlightDBModel extends Model
    {
        protected $connection = 'SpaceFlightDBnew';

        public function getByID($ID, $columns = '*')
        {
            return $this->DB()->get($columns, ['ID' => $ID]);
        }

        public function numOfDataSets($table = null)
        {
            if(!$this->isNull($this->table) && $this->isNull($table))
                return $this->DB()->count(['ID[>=]' => 0]);

            return $this->DB()->count(['ID[>=]' => 0], $table);
        }

        public function numOfDataSetsWhere($where = ['ID[>=]' => 0], $table = null)
        {
            if(!$this->isNull($this->table) && $this->isNull($table))
                return $this->DB()->count($where);

            return $this->DB()->count($where, $table);
        }

        public function getAll($columns = '*')
        {
            return $this->DB()->select($columns, ['ID[>=]' => 0]);
        }

        public function getAllByIDs($IDs, $columns = '*')
        {
            return $this->getAllWhere($columns, ['ID' => $IDs]);
        }

        public function getAllWhere($columns, $where, $table = null)
        {
            if(!$this->isNull($this->table) && $this->isNull($table))
                return $this->DB()->select($columns, $where, $this->table);

            return $this->DB()->select($columns, $where, $table);
        }

        public function getName($ID)
        {
            return $this->getByID($ID, 'Name');
        }
    }