<?php
    namespace app\custom\Models\SpaceFlightDB;


    use app\framework\Component\StdLib\SingletonTrait;

    class Nation extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Nation';
    }