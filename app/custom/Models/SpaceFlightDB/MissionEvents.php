<?php
    namespace app\custom\Models\SpaceFlightDB;


    use app\framework\Component\StdLib\SingletonTrait;

    class MissionEvents extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'MissionEvents';
    }