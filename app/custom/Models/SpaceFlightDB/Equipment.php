<?php
    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class Equipment extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Equipment';

    }