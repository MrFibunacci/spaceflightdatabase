<?php
    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class Power extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'Power';
    }