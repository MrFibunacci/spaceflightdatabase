<?php
    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class LaunchSites extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'LaunchSites';
    }