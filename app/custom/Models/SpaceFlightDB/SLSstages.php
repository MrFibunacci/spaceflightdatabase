<?php
    namespace app\custom\Models\SpaceFlightDB;

    use app\framework\Component\StdLib\SingletonTrait;

    class SLSstages extends SpaceFlightDBModel
    {
        use SingletonTrait;

        protected $table = 'SLSstages';
    }