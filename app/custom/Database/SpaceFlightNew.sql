-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 13, 2018 at 03:26 PM
-- Server version: 10.1.26-MariaDB-1
-- PHP Version: 7.0.25-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `SpaceFlightNew`
--

-- --------------------------------------------------------

--
-- Table structure for table `Applications`
--

CREATE TABLE `Applications` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Applications`
--

INSERT INTO `Applications` (`ID`, `Name`) VALUES
(1, 'Earth Science'),
(2, 'Space Physics'),
(3, 'Life Science'),
(4, 'Solar Physics'),
(5, 'Engineering'),
(6, 'Human Crew'),
(7, 'Technology');

-- --------------------------------------------------------

--
-- Table structure for table `Configuration`
--

CREATE TABLE `Configuration` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Configuration`
--

INSERT INTO `Configuration` (`ID`, `Name`, `Status`) VALUES
(1, 'Pressurized sphere with four antennas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Crew`
--

CREATE TABLE `Crew` (
  `ID` int(11) NOT NULL,
  `Post` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Crew`
--

INSERT INTO `Crew` (`ID`, `Post`) VALUES
(1, '1,2,3');

-- --------------------------------------------------------

--
-- Table structure for table `Equipment`
--

CREATE TABLE `Equipment` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Equipment`
--

INSERT INTO `Equipment` (`ID`, `Name`) VALUES
(1, '2 transmitters');

-- --------------------------------------------------------

--
-- Table structure for table `LaunchSites`
--

CREATE TABLE `LaunchSites` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) COLLATE utf8_bin NOT NULL,
  `Location` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Coordinates` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `BuiltIn` date NOT NULL,
  `UsedUntil` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `LaunchSites`
--

INSERT INTO `LaunchSites` (`ID`, `Name`, `Location`, `Coordinates`, `BuiltIn`, `UsedUntil`) VALUES
(1, 'Goddard Rocket Launching Site', 'Auburn, Massachusetts, United States', '42? 13 6 N, 71? 48 46 W', '1926-00-00', '0000-00-00'),
(2, 'Cape Canaveral Air Force Station', 'Florida', '28.46675°N 80.55852°W', '1956-00-00', '0000-00-00'),
(3, 'Kennedy Space Center', 'Florida', '28.6082°N 80.6040°W', '1963-00-00', '0000-00-00'),
(4, 'Baikonur', 'Tyuratam (Baikonur Cosmodrome), U.S.S.R', '45.6333°N 63.3166°E', '1955-00-00', '0000-00-00'),
(5, 'Wallops Flight Facility', NULL, '37.83°N -75.483°W', '1945-00-00', '0000-00-00'),
(6, 'Point Arguello, United States', ' Santa Barbara County, California', NULL, '1959-03-01', '0000-00-00'),
(7, 'Vandenberg Air Force Base', 'California', '34.737222°N -120.584444°O', '0000-00-00', '0000-00-00'),
(8, 'Tanegashima Space Center', 'Tanegashima', '30.3748266°N, 130.956573°O', '1969-00-00', '0000-00-00'),
(9, 'Centre Spatial Guyanais (CSG)', 'Kourou, French Guiana', NULL, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `MasterMissions`
--

CREATE TABLE `MasterMissions` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) COLLATE utf8_bin NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `MasterMissions`
--

INSERT INTO `MasterMissions` (`ID`, `Name`, `StartDate`, `EndDate`) VALUES
(1, 'Sputnik', '1957-09-04', '1962-10-24'),
(2, 'Explorer', '1958-02-01', NULL),
(3, 'TIROS', '1968-00-00', '1978-00-00'),
(4, 'Vostok', NULL, NULL),
(5, 'Mercury', '1958-00-00', '1963-00-00'),
(6, 'Mariner', '1962-07-22', '1973-00-00'),
(7, 'Apollo', '1961-00-00', '1975-00-00'),
(8, 'Saljut', '1971-04-19', '1980-00-00'),
(9, 'Soyuz', NULL, NULL),
(10, 'Skylab', NULL, NULL),
(11, 'Vanguard', NULL, NULL),
(12, 'Quasi-Zenith Satellite System', '2010-09-11', NULL),
(13, 'International Space Station ', '1998-00-00', '0000-00-00'),
(14, 'Blagovest', NULL, NULL),
(15, 'FORMOSAT', NULL, NULL),
(16, 'Wideband Global Satcom', '2001-00-00', NULL),
(17, 'Tracking and Data Relay Satellite System', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MissionEvents`
--

CREATE TABLE `MissionEvents` (
  `ID` int(11) NOT NULL,
  `Type` varchar(256) COLLATE utf8_bin NOT NULL,
  `Central Body` varchar(256) COLLATE utf8_bin NOT NULL DEFAULT 'Earth',
  `atMissionTime` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `EpochStart` datetime DEFAULT NULL,
  `EpochStop` datetime DEFAULT NULL,
  `FromSpacecraftID` int(11) DEFAULT NULL,
  `Periapsis` float DEFAULT NULL COMMENT 'in km',
  `Apoapsis` float DEFAULT NULL COMMENT 'in km',
  `Period` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'in minutes',
  `Inclination` float DEFAULT NULL,
  `Eccentricity` float DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `RegionsTraversed` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `Description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `MissionEvents`
--

INSERT INTO `MissionEvents` (`ID`, `Type`, `Central Body`, `atMissionTime`, `EpochStart`, `EpochStop`, `FromSpacecraftID`, `Periapsis`, `Apoapsis`, `Period`, `Inclination`, `Eccentricity`, `Latitude`, `Longitude`, `RegionsTraversed`, `Description`) VALUES
(1, 'Orbiter', 'Earth', '4:16:43', '1957-10-04 15:12:00', '1958-01-03 19:00:00', 0, 215, 939, '96.2 min', 65.1, 0.05201, 0, 0, 'Ionosphere,Trapped particle belts', NULL),
(2, 'Orbiter', 'Earth', '12:42:00', '1957-11-03 14:12:00', NULL, NULL, 212, 1660, '103.7 min', 65.33, 0.09892, NULL, NULL, NULL, NULL),
(3, 'Orbiter', 'Earth', NULL, '1958-05-15 03:12:00', NULL, NULL, 217, 1864, '105.9 min', 65.18, 0.11093, NULL, NULL, NULL, NULL),
(4, 'Orbiter', 'Earth', NULL, '1960-05-14 20:00:00', NULL, NULL, 280, 675, '94.25 min', 65.02, 0.02879, NULL, NULL, NULL, NULL),
(5, 'Orbiter', 'Earth', NULL, '1960-08-19 04:38:00', NULL, NULL, 287, 324, '90.72 min', 64.95, 0.00277, NULL, NULL, NULL, NULL),
(6, 'Orbiter', 'Earth', NULL, '1960-12-01 02:26:00', NULL, NULL, 166, 232, '88.47 min', 64.97, 0.00501, NULL, NULL, NULL, NULL),
(7, 'Orbiter', 'Earth', NULL, '1961-02-03 20:18:04', '1961-02-25 19:00:00', NULL, 212, 318, '89.8 min', 64.95, 0.00797, NULL, NULL, NULL, NULL),
(8, 'Orbiter', 'Earth', NULL, '1958-01-31 22:50:00', NULL, NULL, 358, 2550, '114.8 min', 33.24, 0.13985, NULL, NULL, NULL, NULL),
(9, 'Lander', 'Moon', NULL, '1959-09-14 03:30:00', NULL, NULL, 0, 0, '0', 0, 0, 29.1, 0, 'Lithosphere,Planet,The Moon', NULL),
(10, 'Orbiter', 'Earth', NULL, '1959-10-03 22:24:00', '1960-04-27 20:00:00', NULL, 40300, 476500, '22700 min', 73.8, 0.82369, NULL, NULL, NULL, NULL),
(11, 'Orbiter', 'Earth', NULL, '1960-04-01 06:45:00', NULL, NULL, 693, 693, '99.16 min', 48.4, 0.00401, NULL, NULL, NULL, NULL),
(12, 'Orbiter', 'Earth', NULL, '1961-04-12 01:00:00', NULL, NULL, 169, 169, '89.34 min', 64.95, 0.01102, NULL, NULL, NULL, NULL),
(13, 'Suborbital', 'Earth', NULL, '1961-05-05 14:34:13', '1961-05-05 14:49:35', NULL, 0, 187.42, '15 min', 0, 0, NULL, NULL, NULL, NULL),
(14, 'Destroyed', 'Earth', NULL, '1962-07-22 09:26:16', NULL, NULL, 0, 0, '0', 0, 0, NULL, NULL, NULL, NULL),
(15, 'Orbiter', 'Sun', NULL, '1962-08-27 03:21:00', '1962-08-27 03:21:00', NULL, 107710000, 149598000, '292 days', 0, 0.16278, NULL, NULL, 'Interstellar Medium', NULL),
(16, 'Flyby', 'Venus', NULL, '1962-07-22 09:26:16', NULL, NULL, 0, 0, '', 0, 0, NULL, NULL, 'Venus', NULL),
(17, 'Orbiter', 'Sun', NULL, '1962-12-14 14:59:28', NULL, NULL, 107710000, 149598000, '292 days', 0, 0.16278, NULL, NULL, 'Interstellar Medium', NULL),
(18, 'Flyby', 'Venus', NULL, '1961-05-19 20:00:00', NULL, NULL, 99858, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL),
(19, 'Connection Lost', 'Sun', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'Lander', 'Moon', '102:45:39.9 ', '1969-07-20 16:17:41', '1969-07-21 13:54:00', NULL, NULL, NULL, NULL, NULL, NULL, 0.67, 23.47, NULL, NULL),
(21, 'Orbiter', 'Moon', ' 80:11:36.8 ', '1969-07-19 13:21:50', '1969-07-19 17:43:37', NULL, 111, 314, '2.15 ', 0, 0.05024, NULL, NULL, NULL, NULL),
(22, 'Orbiter', 'Moon', '150:29:57.4 ', '1969-07-19 17:43:37', '1969-07-22 00:54:42', NULL, 101, 122, '1.98 ', 0, 0.00568, NULL, NULL, NULL, NULL),
(23, 'Lander', 'Earth', '195:18:35 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13.3, -169.15, NULL, NULL),
(24, 'Lift-off', 'Moon', '124:22:00.8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Undock', 'Moon', '128:03:00 ', NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Docking', 'Moon', '128:03:00 ', NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'Orbiter', 'Earth', NULL, '1971-04-18 19:00:00', NULL, NULL, 200, 222, '88.5 ', 51.6, 0.00167, NULL, NULL, NULL, NULL),
(28, 'Failed Docking', 'Earth', '', '1971-04-23 00:00:00', NULL, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'Orbiter', 'Earth', NULL, '1971-04-22 19:00:00', NULL, NULL, 208, 246, '89 ', 51.6, 0.00287, NULL, NULL, NULL, NULL),
(30, 'Lander', 'Earth', NULL, '1971-04-24 23:40:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 70, NULL, NULL),
(31, 'Orbiter', 'Earth', NULL, '1971-06-05 20:00:00', NULL, NULL, 185, 217, '88.3 ', 51.6, 0.00243, NULL, NULL, NULL, NULL),
(32, 'Docking', 'Earth', NULL, '1971-06-07 00:00:00', '1971-06-07 03:19:00', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'Undock', 'Earth', NULL, '1971-06-29 18:28:00', NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'Lander', 'Earth', NULL, '1971-06-30 02:16:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 47.3566, 70.1214, NULL, NULL),
(35, 'Crew died', 'Earth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'Docking', 'Earth', NULL, '1971-06-07 00:00:00', '1971-06-07 03:19:00', 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'Undock', 'Earth', NULL, '1971-06-29 18:28:00', NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'Destroyed', 'Earth', '175:00:00:00', '1971-10-11 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 169, 178, NULL, 32.56, NULL, NULL, NULL, NULL, NULL),
(40, 'Undock', 'Moon', '110:08:00', NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Lander', 'Moon', '113:02:45', '1972-12-11 14:54:57', '1972-12-14 17:54:37', NULL, NULL, NULL, NULL, NULL, NULL, 20.19, 30.77, NULL, NULL),
(42, 'Lift-off', 'Moon', '188:01:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Docking', 'Moon', '190:17:34', NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Undock', 'Moon', '110:08:00', NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Docking', 'Moon', '190:17:34', NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Seperation', 'Moon', '194:03:31', NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Impact', 'Moon', '195:52:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'De-Orbiting', 'Moon', '195:38:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Seperation', 'Moon', '194:03:31', NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Intersection burn', 'Earth', '236:42:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Splashdown', 'Earth', '304:31:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17.88, 166.11, NULL, NULL),
(52, 'Orbiter', 'Earth', '4:00:00 ', '1973-05-14 13:30:00', NULL, NULL, 434, 442, '93.4 ', 50, 0.00059, NULL, NULL, NULL, NULL),
(53, 'Orbiter', 'Earth', '', '1973-05-25 09:00:00', NULL, NULL, 428, 438, '93.2 ', 50, 0.00073, NULL, NULL, NULL, NULL),
(54, 'Docking', 'Earth', '1:08:56:00', '1973-03-26 21:56:00', NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Lander', 'Earth', NULL, '1973-06-22 13:49:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24.75, -127.033, NULL, NULL),
(56, 'Undock', 'Earth', '28:06:48:07', '1973-06-22 19:48:07', NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Docking', 'Earth', '12:04:26:00', '1973-05-26 21:56:00', NULL, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Undock', 'Earth', '39:02:18:07', '1973-06-22 19:48:07', NULL, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Failure', 'Earth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Orbiter', 'Earth', NULL, '1958-03-26 12:31:00', NULL, NULL, 186, 2799, '115.7 ', 33.38, 0.16589, NULL, NULL, NULL, NULL),
(61, 'Burned up while reentry', 'Earth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Orbiter', 'Earth', NULL, '1958-07-26 11:07:00', NULL, NULL, 263, 2213, '110.2 ', 50.3, 0.1279, NULL, NULL, NULL, NULL),
(63, 'Burned up while reentry', 'Earth', '454:00:00:00', '1959-10-23 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Failure', 'Earth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Orbiter', 'Earth', NULL, '1959-08-07 10:23:00', NULL, NULL, 237, 41900, '754 ', 47, 0.75885, NULL, NULL, NULL, NULL),
(66, 'Connection Lost', 'Earth', NULL, '1959-10-06 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'Burned up while reentry', 'Earth', NULL, '1961-06-30 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Orbiter', 'Earth', NULL, '1959-10-13 11:36:00', NULL, NULL, 573, 1073, '101.38 ', 50.27, 0.03469, NULL, NULL, NULL, NULL),
(69, 'Failed', 'Earth', NULL, '1961-08-24 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Orbiter', 'Earth', NULL, '1960-11-03 00:16:00', NULL, NULL, 417, 2288, '112.7 ', 50, 0.12094, NULL, NULL, NULL, NULL),
(71, 'Failed ', 'Earth', NULL, '1960-12-27 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Orbiter', 'Earth', NULL, '1961-02-16 08:12:00', '1964-04-08 19:00:00', NULL, 545, 2225, '118.6 ', 38.91, 0.10814, NULL, NULL, NULL, NULL),
(73, 'Burned up while reentry', 'Earth', NULL, '1964-04-09 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'Orbiter', 'Earth', NULL, '1961-03-25 00:00:00', NULL, NULL, 220, 180999, '83.50 ', 33, 0, NULL, NULL, NULL, NULL),
(75, 'Battarie run out', 'Earth', '52:00:00', '1961-03-27 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 'Orbiter', 'Earth', NULL, '1961-04-27 09:10:00', NULL, NULL, 486, 1786, '108.1 ', 28.9, 0.08645, NULL, NULL, NULL, NULL),
(77, 'Contact Lost', 'Earth', NULL, '1961-11-17 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'Orbiter', 'Earth', NULL, '1961-08-15 23:21:00', NULL, NULL, 790, 76620, '1587 ', 33.4, 0.84091, NULL, NULL, NULL, NULL),
(79, 'Orbiter', 'Earth', NULL, '1961-08-25 15:26:00', NULL, NULL, 125, 1164, '97.5 ', 37.7, 0.07392, NULL, NULL, NULL, NULL),
(80, 'Orbiter', 'Earth', '4:00:00', '1962-10-02 17:30:00', NULL, NULL, 2601, 96189, '2184.6', 42.8, 0.83893, NULL, NULL, NULL, NULL),
(81, 'Failed', 'Earth', '26:21:30:00', '1961-12-06 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'Orbiter', 'Earth', '4:00:00', '1962-10-27 19:17:00', NULL, NULL, 300, 17438, '311.4 ', 18, 0.56183, NULL, NULL, NULL, NULL),
(83, 'Orbiter', 'Earth', NULL, '1964-12-21 04:00:00', NULL, NULL, 171, 8545, '184.5 ', 18.1, 0.38982, NULL, NULL, NULL, NULL),
(84, 'Failed', 'Earth', NULL, '1967-05-11 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 'Orbiter', 'Earth', NULL, '1962-12-16 09:38:00', NULL, NULL, 750, 1181, '104.3 ', 52, 0.02933, NULL, NULL, NULL, NULL),
(86, 'Failed', 'Earth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'Battarie Failure', 'Earth', NULL, '1963-07-10 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 'Burned while reentry', 'Earth', '', '1966-11-24 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 'Orbiter', 'Earth', NULL, '1963-04-02 20:55:00', NULL, NULL, 255, 916, '96.39 ', 57.6, 0.04743, NULL, NULL, NULL, NULL),
(90, 'Orbiter', 'Earth', NULL, '1963-11-26 21:24:00', NULL, NULL, 4395, 192003, '5606 ', 35.2, 0.89694, NULL, NULL, NULL, NULL),
(91, 'Power supply Failure', 'Earth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 'Orbiter', 'Earth', NULL, '1963-12-19 13:43:00', NULL, NULL, 590, 2394, '115.9 ', 78.6, 0.11454, NULL, NULL, NULL, NULL),
(93, 'Burned while reentry', 'Earth', NULL, '1981-05-10 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 'Orbiter', 'Earth', '', '1964-08-25 09:43:00', NULL, NULL, 864, 1025, '104 ', 79.9, 0.01099, NULL, NULL, NULL, NULL),
(95, 'Connection Lost', 'Earth', NULL, '1965-12-20 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 'Orbiter', 'Earth', NULL, '1964-10-03 23:50:00', NULL, NULL, 917, 94288, '2080', 33.7, 0.86478, NULL, NULL, NULL, NULL),
(97, 'Orbiter', 'Earth', NULL, '1964-10-09 23:07:00', NULL, NULL, 889, 1081, '104.8', 79.7, 0.01303, NULL, NULL, NULL, NULL),
(98, 'Orbiter', 'Earth', NULL, '1964-11-06 07:00:00', NULL, NULL, 451, 865, '97.9', 51.9, 0.0294, NULL, NULL, NULL, NULL),
(99, 'Orbiter', 'Earth', NULL, '1964-11-21 12:17:00', NULL, NULL, 525, 2498, '116.3', 81.4, 0.12496, NULL, NULL, NULL, NULL),
(100, 'Orbiter', 'Earth', NULL, '1964-11-21 12:17:00', NULL, NULL, 522, 2494, '116.3', 81.4, 0.12495, NULL, NULL, NULL, NULL),
(101, 'Orbiter', 'Earth', NULL, '1965-04-29 10:24:00', NULL, NULL, 927, 1320, '107.7', 41.1, 0.02618, NULL, NULL, NULL, NULL),
(102, 'Orbiter', 'Earth', NULL, '1965-05-29 08:00:00', NULL, NULL, 32290, 227456, '8,341.9', 53.6, 0.71617, NULL, NULL, NULL, NULL),
(103, 'Orbiter', 'Earth', NULL, '1965-11-06 13:43:00', NULL, NULL, 1113, 2275, '120.3', 59.4, 0.07193, NULL, NULL, NULL, NULL),
(104, 'Orbiter', 'Earth', NULL, '1965-11-18 23:48:00', NULL, NULL, 704, 891, '100.8', 59.7, 0.01302, NULL, NULL, NULL, NULL),
(105, 'Contact Lost', 'Earth', NULL, '1967-05-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 'Orbiter', 'Earth', NULL, '1966-05-25 09:55:00', '1985-02-22 00:00:00', NULL, 276, 2725, '116', 64.67, 0.15532, NULL, NULL, NULL, NULL),
(107, 'Deorbit', 'Earth', NULL, '1985-02-22 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Caused by battery failure'),
(108, 'Orbiter', 'Earth', NULL, '1966-07-01 12:04:00', NULL, NULL, 265680, 480763, '38792', 24.4, 0.2833, NULL, NULL, NULL, NULL),
(109, 'Connection Lost', 'Earth', NULL, '1971-09-21 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 'Suborbit', 'Earth', NULL, '1966-02-26 16:12:01', '1966-02-26 16:49:21', NULL, NULL, NULL, NULL, NULL, NULL, -8.93333, -10.7167, NULL, 'Recovered by USS Boxer'),
(111, 'Suborbit', 'Earth', NULL, '1966-08-25 17:15:32', '1966-08-25 18:48:34', NULL, NULL, 1142.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 'Splashdown', 'Earth', NULL, '1966-08-25 18:48:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16.1167, 168.9, NULL, 'Recovered by USS Hornet'),
(113, 'Failed', 'Earth', NULL, '1967-01-27 23:31:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Fire wich killed the crew through \"plugs-out\" test'),
(114, 'Orbiter', 'Earth', NULL, '1967-11-08 19:00:00', '1967-11-09 20:37:00', NULL, 183, 188, '88.3', 32.6, 0.00038, NULL, NULL, NULL, NULL),
(115, 'Splashdown', 'Earth', NULL, '1967-11-09 20:37:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30.1, -172.533, NULL, 'Recovered by USS Bennington'),
(116, 'Orbiter', 'Earth', NULL, '1968-01-21 19:00:00', '1968-01-24 00:00:00', NULL, 162, 214, '89.5', 31.6, 0.00396, NULL, NULL, NULL, NULL),
(117, 'Deactivated', 'Earth', NULL, '1968-01-23 09:58:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 'Burned up while reentry', 'Earth', NULL, '1968-01-24 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Uncontrolled reentry'),
(119, 'Orbiter', 'Earth', NULL, '1968-01-21 19:00:00', '1968-02-12 00:00:00', NULL, 163, 222, '88.4', 31.63, 0.00449, NULL, NULL, NULL, NULL),
(120, 'Deactivated', 'Earth', NULL, '1968-01-23 09:58:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 'Burned up while reentry', 'Earth', NULL, '1968-02-12 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Uncontrolled reentry'),
(122, 'Orbiter', 'Earth', NULL, '1968-04-03 19:00:00', '1968-04-04 00:00:00', NULL, 205, 392, '88.2', 32.5, 0.01399, NULL, NULL, NULL, NULL),
(123, 'Splashdown', 'Earth', NULL, '1968-04-04 21:57:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27.6667, -157.917, NULL, 'Recovered by USS Okinawa'),
(124, 'Orbiter', 'Earth', NULL, '1968-10-11 11:02:45', '1968-10-22 07:11:48', NULL, 231, 297, '89.78', 31.63, 0.00496, NULL, NULL, NULL, NULL),
(125, 'Splashdown', 'Earth', NULL, '1968-10-22 11:11:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27.5333, -64.0667, NULL, 'Recovered by USS Essex'),
(126, 'Orbiter', 'Earth', NULL, '1969-03-03 11:48:00', NULL, NULL, 203, 203, '88.64', 32.57, 0.00197, NULL, NULL, NULL, NULL),
(127, 'Seperation', 'Earth', '02:41:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'From Launch system'),
(128, 'Docking', 'Earth', '03:01:59.3', NULL, NULL, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 'Docking', 'Earth', '03:01:59.3', NULL, NULL, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 'Seperation', 'Earth', '04:08:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Spacecraft seperation from launch system'),
(131, 'Undocking', 'Earth', '92:39:36', NULL, NULL, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 'Undocking', 'Earth', '92:39:36', NULL, NULL, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 'Docking', 'Earth', '99:02:26', NULL, NULL, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 'Docking', 'Earth', '99:02:26', NULL, NULL, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 'Splashdown', 'Earth', '241:00:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.25, -67.9333, NULL, 'Recovered by USS Guadalcanal'),
(136, 'EVA', 'Earth', NULL, '1969-03-06 16:45:00', '1969-03-06 17:52:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Schweickart - EVA - LM forward hatch'),
(137, 'EVA', 'Earth', NULL, '1969-03-06 17:01:00', '1969-03-06 18:02:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Scott - Standup EVA - CM side hatch'),
(138, 'Suborbital', 'Earth', NULL, NULL, NULL, NULL, NULL, 203.6, NULL, NULL, NULL, NULL, NULL, NULL, 'Landing in the Atlantic Ocean.'),
(139, 'Suborbital', 'Earth', NULL, NULL, NULL, NULL, NULL, 195, NULL, NULL, NULL, NULL, NULL, NULL, 'Landing in the Atlantic Ocean'),
(140, 'Suborbital', 'Earth', NULL, NULL, NULL, NULL, NULL, 175, NULL, NULL, NULL, NULL, NULL, NULL, 'Landing in the Atlantic Ocean'),
(141, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 32618, 38950, NULL, 41, NULL, NULL, NULL, NULL, NULL),
(142, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 32618, 38950, NULL, 41, NULL, NULL, NULL, NULL, NULL),
(143, 'Orbiter', 'Earth', NULL, '1998-11-20 01:40:00', NULL, NULL, 384, 396, '92', 51.6, 0.01538, NULL, NULL, NULL, NULL),
(144, 'Docking', 'Earth', NULL, '2000-07-26 00:00:00', NULL, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Forward'),
(145, 'Docking', 'Earth', NULL, '2000-07-26 00:00:00', NULL, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Aft'),
(146, 'Docking', 'Earth', NULL, '2000-08-08 20:12:56', NULL, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, 'Docking', 'Earth', NULL, '2000-08-08 20:12:56', NULL, 89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Aft'),
(148, 'Undock', 'Earth', NULL, '2000-11-01 07:53:20', NULL, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 'Undock', 'Earth', NULL, '2000-11-01 07:53:20', NULL, 89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Aft'),
(150, 'Docking', 'Earth', NULL, '2000-10-18 03:47:42', NULL, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, 'Undock', 'Earth', NULL, '2000-12-26 00:00:00', NULL, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, 'Docking', 'Earth', NULL, '2000-10-18 03:47:42', NULL, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Nadir'),
(153, 'Undock', 'Earth', NULL, '2000-12-26 00:00:00', NULL, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Nadir'),
(154, 'Docking', 'Earth', NULL, '2000-12-26 11:03:13', NULL, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, 'Undock', 'Earth', NULL, '2001-02-08 11:26:04', NULL, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, 'Docking', 'Earth', NULL, '2000-12-26 11:03:13', NULL, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, 'Undock', 'Earth', NULL, '2001-02-08 11:26:04', NULL, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, 'Deorbit', 'Earth', NULL, '2000-11-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, 'Deorbit', 'Earth', NULL, '2001-02-08 13:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, 'Orbiter', 'Earth', '', '1958-03-17 07:15:41', NULL, NULL, 654, 3969, '134.2', 0.1909, NULL, NULL, NULL, NULL, NULL),
(161, 'Battery ran out', 'Earth', '', '1958-06-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, 'Comunication lost', 'Earth', NULL, '1964-05-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'The solar powered transmitter operated until May 1964 (when the last signals were received in Quito, Ecuador) after which the spacecraft was optically tracked from Earth.'),
(163, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 440, 450, NULL, 97.22, NULL, NULL, NULL, NULL, NULL),
(164, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 720, 724, NULL, 98.35, NULL, NULL, NULL, NULL, NULL),
(165, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 400, 400, NULL, 51.6, NULL, NULL, NULL, NULL, NULL),
(166, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 35786, 35786, '~24h', 0, NULL, NULL, NULL, NULL, 'GEO'),
(167, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 720, 720, NULL, NULL, NULL, NULL, NULL, NULL, 'Sun-synchronous orbit'),
(168, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 35786, 35786, '~24h', 0, NULL, NULL, NULL, NULL, 'GEO'),
(169, 'RMS capture', 'Earth', NULL, '2017-04-22 10:05:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(170, 'Docking', 'Earth', NULL, '2017-04-22 12:39:00', NULL, 105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Nadir\r\n'),
(171, 'Undock', 'Earth', NULL, '2017-06-04 11:05:00', NULL, 105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Nadir\r\n'),
(172, 'RMS release', 'Earth', NULL, '2017-06-04 13:10:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, 'Docking', 'Earth', NULL, NULL, NULL, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Aft'),
(174, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 35786, 35786, '~24h', 0, NULL, NULL, NULL, NULL, 'GEO'),
(175, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 35786, 35786, '~24h', 0, NULL, NULL, NULL, NULL, 'GEO'),
(176, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 35779.7, 35808.8, '1436.1', 0.1, NULL, NULL, NULL, NULL, NULL),
(177, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 35779.4, 35811.2, '1436.2', 7, NULL, NULL, NULL, NULL, NULL),
(178, 'Orbiter', 'Earth', NULL, NULL, NULL, NULL, 400, 400, NULL, 51.6, NULL, NULL, NULL, NULL, NULL),
(179, 'Docking', 'Earth', NULL, '2001-02-28 09:49:47', NULL, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, 'Docking', 'Earth', NULL, '2001-02-28 09:49:47', NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Aft'),
(181, 'Undock', 'Earth', NULL, '2001-04-16 08:48:00', NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Own Dockingport: Aft'),
(182, 'Undock', 'Earth', NULL, '2001-04-16 08:48:00', NULL, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, 'Deorbit', 'Earth', NULL, '2001-04-16 14:11:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Missions`
--

CREATE TABLE `Missions` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `StartDateTime` datetime DEFAULT NULL,
  `EndDateTime` datetime DEFAULT NULL,
  `LaunchSiteID` int(11) DEFAULT NULL,
  `SpaceLaunchSystemID` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `PayloadIDs` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `MasterMissionIDs` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Crew` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `byAgency` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Description` text COLLATE utf8_bin,
  `outcome` varchar(512) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Missions`
--

INSERT INTO `Missions` (`ID`, `Name`, `StartDateTime`, `EndDateTime`, `LaunchSiteID`, `SpaceLaunchSystemID`, `PayloadIDs`, `MasterMissionIDs`, `Crew`, `byAgency`, `Description`, `outcome`) VALUES
(2, 'Sputnik 1 (PS-1 #1)', '1957-09-04 19:28:43', '1958-01-04 00:00:00', 4, '1', '1', '1', NULL, 'Unknown/U.S.S.R.', NULL, '1'),
(3, 'Sputnik 2 (PS-2 #1)', '1957-11-03 02:30:00', '1958-04-14 00:00:00', 4, '1', '2', '1', '2', 'Unknown/U.S.S.R.', NULL, 'Partial launch failure,\r\nPartial spacecraft failure'),
(4, 'Sputnik 3 (D-1 #2)', '1958-05-15 07:12:00', '1960-04-06 00:00:00', 4, '2', '3', '1', NULL, 'Unknown/U.S.S.R.', NULL, '1'),
(5, 'Sputnik 4', '1960-05-15 00:00:00', '1962-09-05 00:00:00', 4, '3', '4', '1', NULL, 'Unknown/U.S.S.R.', NULL, '1'),
(6, 'Sputnik 5', '1960-08-19 08:38:00', '1960-08-20 00:00:00', 4, '3', '5', '1', NULL, 'Unknown/U.S.S.R', NULL, '1'),
(7, 'Sputnik 6', '1960-12-01 07:27:00', '1960-12-02 19:00:00', 4, '3', '6', '1', NULL, 'Unknown/U.S.S.R', NULL, '1'),
(8, 'Sputnik 7', '1961-02-04 01:18:04', '1961-02-26 00:00:00', 4, '4', '7', '1', NULL, 'Unknown/U.S.S.R', NULL, '1'),
(9, 'Venera 1', '1961-02-12 02:09:00', '1961-02-19 00:00:00', 4, '4', '17', '', NULL, 'Unknown/U.S.S.R.', NULL, '1'),
(10, 'Explorer 1', '1958-02-01 00:00:00', '1970-05-31 00:00:00', 2, '5', '8', '2', NULL, 'Department of Defense-Department of the Army (United States)', NULL, '1'),
(11, 'Luna 2\n', '1959-09-12 00:00:00', '1959-09-14 00:00:00', 4, '6', '9', NULL, NULL, 'Unknown/U.S.S.R', NULL, NULL),
(12, 'Luna 3', '1959-10-04 02:24:00', '1960-04-20 00:00:00', 4, '3', '10', NULL, NULL, 'Unknown/(U.S.S.R)', NULL, NULL),
(13, 'TIROS 1\n', '1960-04-01 11:45:00', '1960-06-15 00:00:00', 2, '7', '11', '3', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(14, 'Vostok 1\n', '1961-04-12 06:07:00', '1961-04-12 00:00:00', 4, '3', '12', '4', '3', 'Unknown/(U.S.S.R)', NULL, NULL),
(15, 'Mercury Redstone 3\n', '1961-05-05 14:34:13', '1961-05-05 14:49:35', 2, '8', '13', '5', '4', 'NASA-Office of Manned Space Flight (United States)', NULL, NULL),
(16, 'Mariner 1', '1962-07-22 09:21:23', '1962-07-22 09:26:16', 2, '9', '14', '6', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(17, 'Mariner 2', '1962-08-27 06:53:14', '1963-01-03 00:00:00', 2, '9', '15', '6', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(18, 'Apollo 8', '1968-12-21 12:51:00', '1968-12-27 00:00:00', 3, '10', '16', '7', '6,7,8', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(19, 'Apollo 11', '1969-07-16 13:32:00', '1969-07-24 00:00:00', 3, '10', '18,19', '7', '1', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(20, 'Salyut 1', '1971-04-19 01:40:00', '1971-10-11 00:00:00', 4, '11', '20', '8', '15,16,17', 'Soviet Academy of Sciences (U.S.S.R)', NULL, NULL),
(21, 'Soyuz 10', '1971-04-23 23:54:00', '1971-04-24 23:40:00', 4, '4', '21', '9', '12,13,14', 'Unknown (U.S.S.R)', NULL, NULL),
(22, 'Soyuz 11', '1971-06-06 04:55:00', '1971-06-29 02:16:52', 4, '6', '22', '9', '15,16,17', 'Unknown (U.S.S.R)', NULL, NULL),
(23, 'Apollo 17', '1972-12-07 05:33:00', '1972-12-19 19:24:59', 3, '10', '23,24', '7', '18,19,20', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(24, 'Skylab 1', '1973-05-14 17:30:00', '1979-07-11 00:00:00', 2, '10', '25', '10', NULL, 'NASA-Office of Manned Space Flight (United States)', NULL, NULL),
(25, 'Skylab 2', '1973-05-25 13:00:00', '1973-06-22 13:49:48', 3, '12', '26', '10', '21,22,23', 'NASA-Office of Manned Space Flight (United States)', NULL, NULL),
(26, 'Sputnik 9', '1961-03-09 06:29:00', NULL, 4, NULL, '27', '1', NULL, 'Unknown (U.S.S.R)', NULL, NULL),
(27, 'Explorer 2', '1958-03-05 18:28:00', '1958-03-05 00:00:00', 2, '5', '28', '2', NULL, 'Department of Defense-Department of the Army (United States)', NULL, 'Launch failure'),
(28, 'Explorer 3', '1958-03-26 17:31:00', '1958-06-28 00:00:00', 2, '5', '29', '2', NULL, 'Department of Defense-Department of the Army (United States)', NULL, '1'),
(29, 'Explorer 4', '1958-07-26 15:07:00', '1959-10-23 00:00:00', 2, '5', '30', '2', NULL, 'Department of Defense-Department of the Army (United States),\nNASA Jet Proplusion Laboratory (United States)', NULL, '1'),
(30, 'Explorer 5', '1958-08-24 00:00:00', '1958-08-24 00:00:00', 2, '5', '31', '2', NULL, 'Department of Defense-Department of the Air Force (United States)\n', NULL, '1'),
(31, 'Explorer 6', '1959-08-07 14:23:00', '1961-07-01 00:00:00', 2, '13', '32', '2', NULL, 'NASA-Office of Space Science Applications (United States), \nDepartment of Defense-Department of the Air Force (United States)\n', NULL, NULL),
(32, 'Explorer 7', '1959-10-13 15:36:00', '1961-08-24 00:00:00', 2, '14', '33', '2', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(33, 'Explorer 8', '1960-11-03 00:00:00', '2012-03-28 00:00:00', 2, '14', '34', '2', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(34, 'Explorer 9', '1961-02-16 13:12:00', '1964-04-09 00:00:00', 2, '15', '35', '2', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(35, 'Explorer 10', '1961-03-25 15:17:00', '1968-06-01 00:00:00', 2, '16', '36', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(36, 'Explorer 11', '1961-04-27 14:10:00', '1961-11-17 00:00:00', 2, '14', '37', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(37, 'Explorer 12', '1961-08-16 03:21:00', '1963-09-01 00:00:00', 2, '16', '38', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(38, 'Explorer 13', '1961-08-25 19:26:00', '1961-08-28 00:00:00', 5, '17', '39', '2', NULL, 'NASA-Office of Aeronautics and Space Technology (United States)', NULL, NULL),
(39, 'Explorer 14', '1962-10-02 21:30:00', '1966-07-01 00:00:00', 2, '18', '40', '2', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(40, 'Explorer 15', '1962-10-27 23:17:00', NULL, 2, '18', '41', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(41, 'Explorer 16', '1962-12-16 14:38:00', NULL, 5, '15', '43', '2', NULL, 'NASA-Office of Aeronautics and Space Technology (United States)\n', NULL, NULL),
(42, 'Explorer 17', '1963-04-03 01:55:00', '1966-11-24 00:00:00', 2, '19', '44', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(43, 'Explorer 18', '1963-11-27 02:24:00', '1965-12-30 00:00:00', 2, '16', '45', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(44, 'Explorer 19', '1963-12-19 18:43:00', '1981-05-10 00:00:00', 6, '15', '46', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(45, 'Explorer 20', '1964-08-25 13:43:00', NULL, 7, '15', '47', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', NULL, NULL),
(46, 'Explorer 21', '1964-10-04 03:50:00', '1966-01-01 00:00:00', 2, '16', '48', '2', NULL, 'NASA-Office of Space Science Applications (United States)', 'Explorer 21 (IMP 2) was a solar-cell and chemical-battery powered spacecraft instrumented for interplanetary and distant magnetospheric studies of energetic particles, cosmic rays, magnetic fields, and plasmas. Each normal telemetry sequence of 81.9 s in duration consisted of 795 data bits. After every third normal sequence there was an 81.9-s interval of rubidium vapor magnetometer analog data transmission. Initial spacecraft parameters included a local time of apogee at noon, a spin rate of 14.6 rpm, and a spin direction of 41.4-deg right ascension and 47.4-deg declination. The significant deviation of the spin rate and direction from the planned values and the achievement of an apogee of less than half the planned value adversely affected data usefulness. Otherwise, spacecraft systems performed well, with nearly complete data transmission for the first 4 months and for the sixth month after launch. Data transmission was intermittent for other times, and the final transmission occurred on October 13, 1965.⁠⁠⁠⁠', NULL),
(47, 'Explorer 22', '1964-10-10 03:07:00', NULL, 7, '17', '49', '2', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(48, 'Explorer 23', '1964-11-06 12:00:00', '1983-06-29 00:00:00', 5, '15', '50', '2', NULL, 'NASA-Office of Aeronautics and Space Technology (United States)\r\n', 'The Explorer 23 micrometeoroid satellite was the third in the series of S 55 micrometeoroid satellites orbited by NASA. Its purpose was to obtain data on the near-earth meteoroid environment, thus providing an accurate estimate of the probability of penetration in spacecraft structures by meteoroids and allowing a more confident definition of the penetration flux-material thickness relation to be derived. The cylindrically shaped spacecraft, about 61 cm in diameter and 234 cm long, was built around the burned out fourth stage of the Scout launch vehicle, which remained as part of the orbiting satellite. Explorer 23 carried stainless steel pressurized-cell penetration detectors, impact detectors, and cadmium sulfide cell detectors to obtain data on the size, number, distribution, and momentum of dust particles in the near-earth environment. In addition, the spacecraft was designed to provide data on the effects of the space environment on the operation of capacitor penetration detectors and solar-cell power supplies. The spacecraft mass, neglecting the fourth stage vehicle hardware and motor, was 96.4 kg. The spacecraft operated satisfactorily during its 1 year life (November 6, 1964, through November 7, 1965), and all mission objectives were accomplished, except for the cadmium sulfide cell detector experiment, which was damaged on liftoff and provided no data', NULL),
(49, 'Explorer 24', '1964-11-21 17:17:00', '1968-10-18 00:00:00', 7, '15', '51', '2', NULL, 'NASA-Office of Space Science Applications (United States)', 'Explorer 24 was placed in orbit together with Explorer 25 from a single launch vehicle. Explorer 24 was identical in configuration to the previously launched balloon satellites Explorer 9 and 19. The spacecraft was 3.6 m in diameter, was built of alternating layers of aluminum foil and plastic film, and was covered uniformly with 5.1-cm white dots for thermal control. It was designed to yield atmospheric density near perigee as a function of space and time from sequential observations of the sphere\'s position in orbit. To facilitate ground tracking, the satellite carried a 136-MHz tracking beacon. The satellite reentered the earth\'s atmosphere on October 18, 1968.', NULL),
(50, 'Explorer 25', '1964-11-21 17:17:00', NULL, 7, '15', '52', '2', NULL, 'NASA-Office of Space Science Applications (United States)', 'Injun 4 (Explorer 25) was a magnetically aligned satellite launched simultaneously with Explorer 24 (AD-B) (Air Density experiment) using a Scout rocket. The satellite\'s primary mission was to make measurements of the influx of energetic particles into the earth\'s atmosphere and to study atmospheric heating and the increase in scale height which have been correlated with geomagnetic activity. Studies of the natural and artificial trapped radiation belts were also conducted. A biaxial fluxgate magnetometer was used to monitor the orientation of the spacecraft with respect to the local magnetic field. Injun 4 was equipped with a tape recorder and analog-to-digital converters. The satellite power was derived from rechargeable batteries and solar cells. A transmitter operating in an AM mode at carrier frequency 136.29 MHz was used to transmit real-time data, and one operating in a PM mode at 136.86 MHz was used to transmit tape recorder data. Stable magnetic alignment was not achieved until late February 1965. The satellite sent radiation data until December 1966 and is expected to be in orbit for about 200 yr.', NULL),
(51, 'Explorer 26', '1964-12-21 09:00:00', '1967-05-11 00:00:00', 2, NULL, '42', '2', NULL, 'NASA-Office of Space Science Applications (United States)\n', 'Explorer 26 was a spin-stabilized, solar-cell-powered spacecraft instrumented to measure trapped particles and the geomagnetic field. A 16-channel PFM/PM time-division multiplexed telemeter was used. The time required to sample the 16 channels (one frame period) was 0.29 s. Half of the channels were used to convey eight-level digital information. The other channels were used for analog information. During ground processing, the analog information was digitized with an accuracy of 1/800th of full scale. One analog channel was subcommutated in a 16-frame-long pattern and used to telemeter spacecraft temperatures, power system voltages, currents, etc. A digital solar aspect sensor measured the spin period and phase, digitized to 0.036 s, and the angle between the spin axis and sun direction to about 3-deg intervals. The spacecraft systems functioned well, except for some undervoltage turnoffs, until May 26, 1967, when the telemeter failed. The initial spin rate was 33 rpm, and the spin axis direction was right ascension 272.8 deg and declination 21.5 deg. The spin rate decreased with time to 2 rpm on September 9, 1965. For the balance of its life, the spacecraft was coning or tumbling at a rate of about 1 rpm.', NULL),
(52, 'Explorer 27', '1965-04-29 14:24:00', NULL, 5, '15', '53', '2', NULL, 'NASA-Office of Space Science (United States)', 'BE-C (Explorer 27) was a small ionospheric research satellite instrumented with an electrostatic probe, radio beacons, a passive laser tracking reflector, and a Doppler navigation experiment. Its primary objective was to obtain worldwide observations of total electron content between the spacecraft and the earth. The satellite was initially spin stabilized, but it was despun after solar paddle erection. Subsequent stabilization oriented the satellite axis of symmetry with the local magnetic field by means of a strong bar magnet and damping rods. A three-axis magnetometer and sun sensors provided information on the satellite attitude and spin rate. There was no tape recorder aboard so that satellite performance data and electrostatic probe data were observed only when the satellite was within range of a ground telemetry station. Continuous transmitters operated at 162 and 324 MHz to permit precise tracking by \"Transit\" tracking stations for navigation and geodetic studies. The satellite was turned off on July 20, 1973, due to frequency interference with higher priority spacecraft.', NULL),
(53, 'Explorer 28', '1965-05-29 12:00:00', '1968-07-04 00:00:00', 2, '16', '54', '2', NULL, 'NASA-Office of Space Science Applications (United States)', 'Explorer 28 (IMP 3) was a solar-cell and chemical-battery powered spacecraft instrumented for interplanetary and distant magnetospheric studies of energetic particles, cosmic rays, magnetic fields, and plasmas. Initial spacecraft parameters included a local time of apogee of 2020 h, a spin rate of 23.7 rpm, and a spin direction of 64.9-deg right ascension and -10.9-deg declination. Each normal telemetry sequence was 81.9 s in duration and consisted of 795 data bits. After every third normal telemetry sequence there was an 81.9-s interval of rubidium vapor magnetometer analog data transmission. Performance was essentially normal until late April 1967, then intermittent until May 12, 1967, after which no further data were acquired.\r\n\r\n', NULL),
(54, 'Explorer 29', '1965-11-06 18:43:00', NULL, 2, '16', '55', '2', NULL, 'NASA-Office of Space Science (United States)', 'The GEOS 1 (Geodetic Earth Orbiting Satellite) spacecraft was a gravity-gradient-stabilized, solar-cell powered unit designed exclusively for geodetic studies. It was the first successful active spacecraft of the National Geodetic Satellite Program. Instrumentation included (1) four optical beacons, (2) laser reflectors, (3) a radio range transponder, (4) Doppler beacons, and (5) a range and range rate transponder. These were designed to operate simultaneously to fulfill the objectives of locating observation points (geodetic control stations) in a three dimensional earth center-of-mass coordinate system within 10 m of accuracy, of defining the structure of the earth\'s irregular gravitational field and refining the locations and magnitudes of the large gravity anomalies, and of comparing results of the various systems onboard the spacecraft to determine the most accurate and reliable system. Acquisition and recording of data were the responsibility of the GSFC Space Tracking and Data Acquisitions Network (STADAN). Ten major observing networks were used.\r\n\r\nAlternate Names\r\n\r\nGEOS-A\r\nExplorer 29\r\n01726\r\nFacts in Brief\r\n\r\nLaunch Date: 1965-11-06\r\nLaunch Vehicle: Delta\r\nLaunch Site: Cape Canaveral, United States\r\nMass: 387.0 kg\r\n\r\nFunding Agency\r\n\r\nNASA-Office of Space Science (United States)\r\nDiscipline', NULL),
(55, 'Explorer 30', '1965-11-19 04:48:00', '1967-05-00 00:00:00', 5, '15', '56', '2', NULL, 'NASA-Office of Space Science Applications (United States),\r\nDepartment of Defense-Department of the Navy (United States)\r\n', 'The NRL SOLRAD 8 satellite was one of the SOLRAD series that began in 1960 to provide continuous coverage of solar radiation with a set of standard photometers. SOLRAD 8 was a spin-stabilized satellite oriented with its spin axis perpendicular to the sun-satellite line so that the 14 solar X-ray and ultraviolet photometers pointing radially outward from its equatorial belt viewed the sun with each revolution. Data were transmitted in real time by means of an FM/AM telemetry system and were recorded by the stations on the STADAN tracking network. The satellite performed normally except for the spin system, which failed to maintain 60 rpm (at spin rates below 10 rpm data reduction became difficult). The spin rate gradually decreased to 4 rpm on September 12, 1966. At that time, ground command succeeded in reactivating spinup to 78 rpm, which exhausted the gas supply. From this point, the spin rate gradually decreased to 10 rpm in August 1967, when data collection was substantially decreased.\r\n\r\n', NULL),
(56, 'Explorer 31', '1965-11-29 04:48:00', '1967-02-21 00:00:00', 7, NULL, '57', '2', NULL, 'NASA-Office of Space Science Applications (United States)', NULL, NULL),
(57, 'Explorer 32', '1966-05-25 13:55:00', '1985-02-22 00:00:00', 2, '20', '58', '2', NULL, 'NASA-Office of Space Science Applications (United States)', 'Explorer 32 was an aeronomy satellite which was designed to directly measure temperatures, composition, densities, and pressures in the upper atmosphere on a global basis. The satellite was a stainless steel, vacuum-sealed sphere, 0.889 m in diameter. The experimental payload included one ion and two neutral mass spectrometers, three magnetron density gauges, and two electrostatic probes. Additional equipment included optical and magnetic aspect sensors, magnetic attitude and spin rate control systems, and a tape recorder for data acquisition at locations remote from ground receiving stations. Power was supplied by silver-zinc batteries and a solar cell array mounted on the satellite exterior. Two identical pulse-modulated telemetry systems and a canted turnstile antenna were employed. The two neutral-particle mass spectrometers failed about 6 days after launch. The remaining experiments operated satisfactorily and provided useful data for most of the 10-month satellite lifetime. The spacecraft ceased to function due to battery failures which resulted from depressurization of the sphere.', NULL),
(58, 'Explorer 33', '1966-07-01 16:04:00', '1971-09-21 00:00:00', 2, '21', '59', '2', NULL, 'NASA-Office of Space Science Applications (United States)', 'Explorer 33 was a spin-stabilized (spin axis parallel to the ecliptic plane, spin period varying between 2.2 and 3.6 s) spacecraft instrumented for studies of interplanetary plasma, energetic charged particles (electrons, protons, and alphas), magnetic fields, and solar X rays at lunar distances. The spacecraft failed to achieve lunar orbit but did achieve mission objectives. The initial apogee occurred at about 1600 h local time. Over the first 3-yr period, perigee varied between 6 and 44 earth radii. Apogee varied between 70 and 135 earth radii, and the inclination with respect to the equator of the earth varied between 7 and 60 deg. Periods of principal data coverage (essentially 100#%) are July 1, 1966 (launch), to January 14, 1970; February 21, 1970, to March 6, 1970; July 31, 1970, to September 14, 1970; January 15, 1971, to February 28, 1971; March 23, 1971, to May 31, 1971; and August 23, 1971, to September 15, 1971. No data were obtained after September 21, 1971.', NULL),
(59, 'Explorer 34', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(60, 'Explorer 35', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(61, 'Explorer 36', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(62, 'Explorer 37', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(63, 'Explorer 38', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(64, 'Explorer 39', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(65, 'Explorer 40', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(66, 'Explorer 41', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(67, 'Explorer 42', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(68, 'Explorer 43', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(69, 'Explorer 44', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(70, 'Explorer 45', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(71, 'Explorer 46', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(72, 'Explorer 47', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(73, 'Explorer 48', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(74, 'Explorer 49', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(75, 'Explorer 50', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(76, 'Explorer 51', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(77, 'Explorer 52', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(78, 'Explorer 53', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(79, 'Explorer 54', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(80, 'Explorer 55', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(81, 'Explorer 56', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(82, 'Explorer 57', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(83, 'Explorer 58', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(84, 'Explorer 59', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(85, 'Explorer 60', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(86, 'Explorer 61', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(87, 'Explorer 62', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(88, 'Explorer 63', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(89, 'Explorer 64', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(90, 'Explorer 65', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(91, 'Explorer 66', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(92, 'Explorer 67', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(93, 'Explorer 68', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(94, 'Explorer 69', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(95, 'Explorer 70', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(96, 'Explorer 71', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(97, 'Explorer 72', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(98, 'Explorer 73', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(99, 'Explorer 74', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(100, 'Explorer 75', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(101, 'Explorer 76', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL),
(102, 'AS-201', '1966-02-26 16:12:01', '1966-02-26 16:49:21', 2, '12', '60', '7', NULL, 'NASA-Office of Manned Space Flight (United States)\n', 'AS-201 (or SA-201), flown February 26, 1966, was the first unmanned test flight of an entire production Block I Apollo Command/Service Module and the Saturn IB launch vehicle. The spacecraft consisted of the second Block I command module and the first Block I service module. The suborbital flight was a partially successful demonstration of the service propulsion system and the reaction control systems of both modules, and successfully demonstrated the capability of the Command Module\'s heat shield to survive re-entry from low Earth orbit.', NULL),
(103, 'AS-203', '1966-07-05 14:53:00', '1966-07-05 00:00:00', 2, '12', '', '7', NULL, 'NASA-Office of Manned Space Flight (United States)\n', 'AS-203 (or SA-203) was an unmanned flight of the Saturn IB rocket on July 5, 1966. It carried no Apollo Command/Service Module spacecraft, as its purpose was to verify the design of the S-IVB rocket stage restart capability that would later be used in the Apollo program to boost astronauts from Earth orbit to a trajectory towards the Moon. It successfully achieved its objectives, but the stage was inadvertently destroyed after four orbits.\n\n', NULL),
(104, 'AS-202', '1966-08-25 17:15:32', '1966-08-25 18:48:34', 2, '12', '61', '7', NULL, 'NASA-Office of Manned Space Flight (United States)\n', NULL, NULL),
(105, 'Apollo 1', '1967-01-27 23:31:19', '1967-01-27 23:31:19', 2, '12', '62', '7', '24,25,26', 'NASA-Office of Manned Space Flight (United States)\n', 'Planed launch: 1967-02-21', '1'),
(106, 'Apollo 4', '1967-11-09 12:00:01', '1967-11-09 20:37:00', 2, '10', '63', '7', NULL, 'NASA-Office of Manned Space Flight (United States)', 'The unmanned Saturn/Apollo 4 (AS-501) mission was the first all-up test of the three stage Saturn V rocket. It carried a payload of an Apollo Command and Service Module (CSM) into Earth orbit. The mission was designed to test all aspects of the Saturn V launch vehicle and also returned pictures of Earth taken by the automatic Command Module apogee camera from about one hour before to one hour after apogee. Mission objectives included testing of structural integrity, compatibility of launch vehicle and spacecraft, heat shield and thermal seal integrity, overall reentry operations, launch loads and dynamic characteristics, stage separation, launch vehicle subsystems, the emergency detection system, and mission support facilities and operations. The mission was deemed a successful test.\n\nOrbital insertion was achieved by ignition of the third (S-IVB) stage, putting the spacecraft (S-IVB and CSM) into a 184 x 192 km parking orbit with a period of 88.2 minutes and an inclination of 32.6 degrees. After two orbits the S-IVB was re-ignited for a simulated translunar injection burn, putting the spacecraft into an Earth-intersecting trajectory with an apogee of 17,346 km. The S-IVB stage then separated from the CSM, and the service propulsion system (SPS) ignited for 16 seconds, raising the apogee to 18,216 km. Later the SPS was re-ignited for 271 seconds to accelerate the CSM to beyond lunar trajectory return velocities. SPS cutoff was followed by separation of the Command Module (CM) from the Service Module and orientation of the CM for reentry. Atmospheric entry at 122 km occurred at a flight path angle of 7.077 degrees with a velocity of 11,140 meters/second. The CM landed near Hawaii at 20:37 UT 9 November 1967 about 16 km from the target landing point.\n\n', '1'),
(107, 'Apollo 5', '1968-01-22 22:48:09', '1968-02-12 00:00:00', 2, '12', '64,65', '7', NULL, 'NASA-Office of Manned Space Flight (United States)', 'The launch vehicle for Apollo 5 was the Saturn IB, a smaller rocket than the Saturn V but capable of launching an Apollo spacecraft into Earth orbit. The Saturn IB SA-204 used on Apollo 5 was the one originally intended for Apollo 1. It had been undamaged in the fire at Launch Complex 34 and was reassembled at Launch Complex 37B for the Apollo 5 launch.', '1'),
(108, 'Apollo 6', '1968-04-04 12:00:01', '1968-04-04 00:00:00', 2, '10', '66', '7', NULL, 'NASA-Office of Manned Space Flight (United States)', 'Apollo 6 (also known as AS-502), launched on April 4, 1968, was the second A type mission of the United States Apollo program, an unmanned test of the Saturn V launch vehicle. It was also the final unmanned Apollo test mission.\n\n', '1'),
(109, 'Apollo 7', '1968-10-11 15:02:45', '1968-10-22 00:00:00', 2, '12', '67', '7', '27,28,29', 'NASA-Office of Manned Space Flight (United States)\n', NULL, '1'),
(110, 'Apollo 9', '1969-03-03 00:00:00', '1969-03-13 00:00:00', 2, '12', '68,69', '7', '30,31,32', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(111, 'Apollo 10', '1969-05-18 16:49:00', '1969-05-26 16:52:23', 2, '12', '70,71', '7', '33,34,35', 'NASA-Office of Manned Space Flight (United States)', 'Apollo 10 was the fourth manned mission in the United States Apollo space program, and the second (after Apollo 8) to orbit the Moon. Launched on May 18, 1969, it was the F mission: a \"dress rehearsal\" for the first Moon landing, testing all of the components and procedures, just short of actually landing. The Lunar Module (LM) followed a descent orbit to within 8.4 nautical miles (15.6 km) of the lunar surface, at the point where powered descent for landing would normally begin.[2] Its success enabled the first landing to be attempted on the Apollo 11 mission two months later.\n\nAccording to the 2002 Guinness World Records, Apollo 10 set the record for the highest speed attained by a manned vehicle: 39,897 km/h (11.08 km/s or 24,791 mph) on May 26, 1969, during the return from the Moon.', '1'),
(112, 'Apollo 12', '1969-11-14 16:22:00', '1969-11-24 20:58:24', 2, '12', '72,73', '7', NULL, 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(113, 'Apollo 13', '1970-04-11 19:13:00', '1970-04-17 00:00:00', 2, '10', '74,75', '7', '7,36,37', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(114, 'Apollo 14', '1971-01-31 21:03:02', '1971-02-09 21:05:00', 2, '10', '76,77', '7', '38,39,40', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(115, 'Apollo 15', '1971-07-26 13:34:00', '1971-08-07 20:45:53', 2, '10', '78,79', '7', '31,41,42', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(116, 'Apollo 16', '1972-04-16 17:54:00', '1972-04-27 00:00:00', 2, '10', '80,81', '7', '34,43,44', 'NASA-Office of Manned Space Flight (United States)', NULL, '1'),
(118, 'Vanguard TV-0', '1956-12-08 06:03:00', '1956-12-08 00:00:00', 2, '25', '82', '11', NULL, 'United States, Naval Research Laboratory', 'Vanguard TV-0, also called Vanguard Test Vehicle Zero, was the first sub-orbital test flight of a Vanguard rocket as part of the Project Vanguard.', '1'),
(119, 'Vanguard TV-1', '1957-05-01 01:29:00', '1957-05-01 00:00:00', 2, '26', '83', '11', NULL, 'United States, Naval Research Laboratory', 'Vanguard TV-1, also called Vanguard Test Vehicle One, was the Second sub-orbital test flight of a Vanguard rocket as part of the Project Vanguard. VT-1 followed the successful launch of Vanguard VT-0 a one-stage rocket launched in December 1956.', '1'),
(120, 'Vanguard TV-2', '1957-10-23 00:00:00', '1957-10-23 00:00:00', 2, '26', '84', '11', NULL, 'United States, Naval Research Laboratory', '', '1'),
(121, 'Vanguard TV-3', NULL, NULL, NULL, NULL, NULL, '11', NULL, NULL, NULL, NULL),
(122, 'QZS 2', '2017-06-01 17:46:00', NULL, 8, '27', '85', '12', NULL, 'Japan Aerospace Exploration Agency (Japan)', 'Quasi Zenith Satellite, OZS 2 is a Japanese satellite navigation system operating from inclined, elliptical geosynchronous orbits to achieve optimal high-elevation visibility in urban canyons and mountainous areas. The navigation system objective is to broadcast GPS-interoperable and augmentation signals as well as original Japanese (QZSS) signals from a three-spacecraft constellation.', '1'),
(123, 'QZS 1', '2010-09-11 11:17:00', NULL, 8, '27', '86', '12', NULL, 'Japan Aerospace Exploration Agency (Japan)', 'QZS 1, a Japanese navigation satellite also known as Michibiki, was launched on 11 September 2010 from Tanegashima at 11:17 UT. The craft weighed 4100 kg and was launched by an H-2A rocket. QZS 1 is a JAXA satellite and is part of the QZSS (Quazi-Zenith Satellite System) that is aimed at overcoming ground interference by launching more navigation satellites strategically positioned high in the sky above Asia. The QZSS consists of a constellation of three spacecraft. QZS 1 Michibiki is phase 1 of the QZSS project. Michibiki is demonstrating the system\'s navigation instruments and proving the satellite works before Japan commits to launching the other two spacecraft. Michibiki will circle the Earth at a 45° inclination to the equator and will eventually park more than 20,000 miles above Earth. The satellite has a design life of 10 years.', '1'),
(124, 'ISS-1A/R', '1998-11-20 06:40:00', NULL, 4, '11', '87', '13', NULL, 'Russian Space Agency (Russia), National Aeronautics and Space Administration (United States)', 'Zarya (Dawn) is the Russian-built, American-financed module that was the first-launched of numerous planned modules of the International Space Station (ISS).', '1'),
(125, 'ISS-1R', '2000-07-12 04:56:00', NULL, 4, '11', '88', '13', NULL, 'Unknown (Russia)', NULL, '1'),
(126, 'ISS-1P', '2000-08-06 16:26:42', '2000-11-01 07:53:20', 4, '28', '89', '13', NULL, 'Unknown (Russia)', NULL, '1'),
(127, 'ISS-2P', '2000-11-16 01:33:00', '2001-02-08 13:50:00', 4, '28', '90', '13', NULL, 'Unknown (Russia)', NULL, '1'),
(128, 'ISS-3P', '2001-02-26 08:09:35', '2001-04-16 14:11:00', 4, '28', '109', '13', NULL, 'Unknown (Russia)', NULL, '1'),
(129, 'ISS-4P', '2001-05-21 22:42:00', '2001-08-22 00:00:00', 4, '33', '130', '13', NULL, NULL, NULL, '1'),
(130, 'ISS-3P', '2001-08-21 09:23:00', '2001-11-22 00:00:00', NULL, '28', '131', '13', NULL, NULL, NULL, '1'),
(131, 'ISS-4R', '2001-09-14 23:35:00', NULL, NULL, '28', '110,111', '13', NULL, NULL, NULL, '1'),
(132, 'ISS-6P', '2001-11-26 18:24:00', NULL, NULL, '33', '112', '13', NULL, NULL, NULL, '1'),
(133, 'ISS-7P', '2002-03-21 20:13:00', NULL, NULL, '28', '113', '13', NULL, NULL, NULL, '1'),
(134, 'ISS-8P', '2002-06-26 05:36:00', NULL, NULL, '28', '114', '13', NULL, NULL, NULL, '1'),
(135, 'ISS-9P', '2002-09-25 16:58:00', NULL, NULL, '33', '115', '13', NULL, NULL, NULL, '1'),
(136, 'ISS-10P', '2003-02-02 12:59:00', NULL, NULL, '28', '116', '13', NULL, NULL, NULL, '1'),
(137, 'ISS-11P', '2003-07-08 10:34:00', NULL, NULL, '28', '119', '13', NULL, NULL, NULL, '1'),
(138, 'ISS-12P', '2003-08-29 01:48:00', NULL, NULL, '28', '120', '13', NULL, NULL, NULL, '1'),
(139, 'ISS-13P', '2004-01-29 11:58:00', NULL, NULL, '28', '121', '13', NULL, NULL, NULL, '1'),
(140, 'ISS-14P', '2004-05-25 12:34:00', NULL, NULL, '28', '122', '13', NULL, NULL, NULL, '1'),
(141, 'ISS-15P', '2004-08-11 05:03:00', NULL, NULL, '28', '123', '13', NULL, NULL, NULL, '1'),
(142, 'ISS-16P', '2004-12-23 22:19:00', NULL, NULL, '28', '124', '13', NULL, NULL, NULL, '1'),
(143, 'ISS-17P', '2005-02-28 19:09:00', NULL, NULL, '28', '125', '13', NULL, NULL, NULL, '1'),
(144, 'ISS-18P', '2005-06-16 23:09:00', NULL, NULL, '28', '126', '13', NULL, NULL, NULL, '1'),
(145, 'ISS-19P', '2005-09-08 09:08:00', NULL, NULL, '28', '127', '13', NULL, NULL, NULL, '1'),
(146, 'ISS-20P', '2005-12-21 18:38:00', NULL, NULL, '28', '128', '13', NULL, NULL, NULL, '1'),
(147, 'ISS-21P', '2006-04-24 16:03:00', NULL, NULL, '28', '129', '13', NULL, NULL, NULL, '1'),
(148, 'ISS-22P', '2006-06-24 15:08:00', NULL, NULL, '28', '132', '13', NULL, NULL, NULL, '1'),
(149, 'ISS-23P', '2006-10-23 13:41:00', NULL, NULL, '28', '133', '13', NULL, NULL, NULL, '1'),
(150, 'ISS-24P', '2007-01-18 02:12:00', NULL, NULL, '28', '134', '13', NULL, NULL, NULL, '1'),
(151, 'ISS-25P', '2007-05-12 03:25:00', NULL, NULL, '28', '135', '13', NULL, NULL, NULL, '1'),
(152, 'ISS-26P', '2007-08-02 17:34:00', NULL, NULL, '28', '136', '13', NULL, NULL, NULL, '1'),
(153, 'ISS-27P', '2007-12-23 07:12:00', NULL, NULL, '28', '137', '13', NULL, NULL, NULL, '1'),
(154, 'ISS-28P', '2008-02-05 13:02:00', NULL, NULL, '28', '138', '13', NULL, NULL, NULL, '1'),
(155, 'ISS-29P', '2008-05-14 20:22:00', NULL, NULL, '28', NULL, '13', NULL, NULL, NULL, '1'),
(156, 'ISS-30P', '2008-09-10 19:50:00', NULL, NULL, '28', NULL, '13', NULL, NULL, NULL, '1'),
(157, 'ISS-ATV-1', '2008-03-09 04:03:00', NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, '1'),
(158, 'ISS-31P', '2008-11-26 12:38:00', NULL, NULL, '28', NULL, '13', NULL, NULL, NULL, '1'),
(159, 'Progress M-64', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(160, 'Progress M-65', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(161, 'Progress M-66', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(162, 'Progress M-01M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(163, 'Progress M-02M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(164, 'Progress M-67', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(165, 'H-2 Transfer Vehicle HTV-1', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(166, 'Progress M-03M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(167, 'Poisk', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(168, 'Progress M-MIM2', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(169, 'Progress M-04M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(170, 'Progress M-05M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(171, 'Progress M-06M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(172, 'Progress M-07M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(173, 'Progress M-08M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(174, 'H-2 Transfer Vehicle HTV-2', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(175, 'Progress M-09M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(176, 'Automated Transfer Vehicle ATV-2', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(177, 'Progress M-10M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(178, 'Progress M-11M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(179, 'Progress M-13M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(180, 'Progress M-14M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(181, 'Automated Transfer Vehicle ATV-3', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(182, 'Progress M-15M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(183, 'Dragon C2+', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(184, 'H-2 Transfer Vehicle HTV-3', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(185, 'Progress M-16M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(186, 'Dragon CRS 1', '2012-10-08 00:34:07', '2012-10-28 19:22:00', 2, '22', NULL, '13', NULL, NULL, NULL, '1'),
(187, 'Progress M-17M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(188, 'Progress M-18M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(189, 'Dragon CRS 2', NULL, NULL, NULL, '22', NULL, '13', NULL, NULL, NULL, '1'),
(190, 'Progress M-19M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(191, 'Automated Transfer Vehicle ATV 4', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(192, 'Progress-M 20M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(193, 'H-2 Transfer Vehicle HTV 4', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(194, 'Cygnus COTS 1', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(195, 'Progress-M 21M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(196, 'Cygnus CRS 1', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(197, 'Progress-M 22M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(198, 'Progress-M 23M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(199, 'Dragon CRS 3', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(200, 'Cygnus CRS 2', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(201, 'Progress-M 24M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(202, 'Automated Transfer Vehicle ATV 5', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(203, 'Dragon CRS 4', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(204, 'Cygnus CRS 3', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(205, 'Progress-M 25M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(206, 'Dragon CRS 5', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(207, 'Progress-M 26M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(208, 'Dragon CRS 6', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(209, 'Progress M-27M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(210, 'Dragon CRS 7', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(211, 'Progress-M 28M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(212, 'H-2 Transfer Vehicle HTV 5', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(213, 'Progress-M 29M', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(214, 'Cygnus OA-4', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(215, 'Progress-MS 01', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(216, 'Cygnus OA-6', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(217, 'Progress-MS 02', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(218, 'Dragon CRS 8', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(219, 'BEAM', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(220, 'Progress-MS 03', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(221, 'Dragon CRS 9', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(222, 'Cygnus OA-5', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(223, 'Progress-MS 04', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(224, 'H-2 Transfer Vehicle HTV 6', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(225, 'Dragon CRS 10', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(226, 'Progress-MS 05', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL),
(227, 'Cygnus OA-7', '2017-04-18 15:11:26', '2017-06-11 17:08:00', 2, '32', '104', '13', NULL, 'National Aeronautics and Space Administration (United States)', NULL, '1'),
(228, 'Dragon CRS 11', NULL, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, ''),
(229, 'QZS 3', '2017-08-18 05:30:00', NULL, 8, '29', '91', '12', NULL, 'Japan Aerospace Exploration Agency (Japan)', NULL, '1'),
(230, 'Vanguard 1', '1958-03-17 12:15:41', NULL, 2, '26', '92', '11', NULL, 'Department of Defense-Department of the Navy (United States)', NULL, 'Failure'),
(231, 'Sputnik (3) (D-1 #1)', '1958-04-27 00:00:00', NULL, 4, '2', NULL, '1', NULL, 'USSR', NULL, 'Failure'),
(232, 'OPTSAT-3000 & VENµS', '2017-08-02 01:58:00', NULL, 9, '30', '93,94', NULL, NULL, 'ESA', NULL, '1'),
(233, 'Dragon CRS 12', '2017-08-14 16:31:00', NULL, 2, '24', '95,96,97,98,99,100', '13', NULL, NULL, NULL, '1'),
(234, 'Blagovest 1', '2017-08-16 22:06:00', NULL, 4, '31', '101', '14', NULL, 'Unknown (Russia)', NULL, '1'),
(235, 'TDRS 13', '2017-08-18 12:29:00', NULL, 2, '32', '102', NULL, NULL, 'National Aeronautics and Space Administration (United States)', NULL, '1'),
(236, 'FORMOSAT 5', '2017-08-24 00:00:00', NULL, 7, '24', '103', '15', NULL, NULL, NULL, '1'),
(237, 'SES-10', '2017-03-30 22:27:00', NULL, 3, '24', '106', NULL, NULL, NULL, NULL, '1'),
(238, 'WGS 9', '2017-03-19 00:18:00', NULL, 2, NULL, '107', '16', NULL, 'United States Air Force Academy (International)', 'WGS-9, which was purchased for the Air Force by a group of other nations in exchange for access to the WGS system, will join the eight satellites already in orbit which launched between 2007 and 2016. Built by Boeing, WGS satellites are based on the BSS-702 platform and designed for fourteen years of service. Each spacecraft is equipped with an Aerojet Rocketdyne R-4D-15 High Performance Apogee Thruster (HiPAT) to perform insertion into geosynchronous orbit and four Xenon-Ion Propulsion System (XIPS-25) thrusters for stationkeeping. The WGS-9 satellite carries X and Ka-band transponders. The satellite will use a phased array antenna to provide eight jam-resistant X-band beams, while ten individual antennae will provide Ka-band beams. An additional X-band payload will be used to provide Earth coverage. The satellite can support 8.088 gigahertz of bandwidth, with an expected downlink speed of up to 11 Gbps.', '1'),
(239, 'TDRS-13', '2017-08-18 12:29:00', NULL, 2, '32', '108', '17', NULL, 'National Aeronautics and Space Administration (United States)', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `Nation`
--

CREATE TABLE `Nation` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Nation`
--

INSERT INTO `Nation` (`ID`, `Name`) VALUES
(1, 'U.S.S.R. / Soviet Union'),
(2, 'United States of America'),
(3, 'Japan'),
(4, 'German');

-- --------------------------------------------------------

--
-- Table structure for table `Payloads`
--

CREATE TABLE `Payloads` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) COLLATE utf8_bin NOT NULL,
  `Application` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Mission` varchar(256) COLLATE utf8_bin NOT NULL,
  `NSSDCA ID` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `SATCATno` int(11) DEFAULT NULL,
  `Mass` float DEFAULT NULL COMMENT 'in kg',
  `Equipment` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Propulsion` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Power` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Lifetime` int(11) NOT NULL,
  `Alternate Names` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `Operator` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `Contractor` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `MissionEventIDs` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Nation` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Configuration` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Payloads`
--

INSERT INTO `Payloads` (`ID`, `Name`, `Application`, `Mission`, `NSSDCA ID`, `SATCATno`, `Mass`, `Equipment`, `Propulsion`, `Power`, `Lifetime`, `Alternate Names`, `Operator`, `Contractor`, `MissionEventIDs`, `Nation`, `Configuration`, `Status`) VALUES
(1, 'Sputnik 1', '1,2,7', 'Earth Science,Space Physics', '1957-001B', 2, 83.6, '1', '18', '1', 0, NULL, NULL, 'NPO Energia', '1', '1', 1, 2),
(2, 'Sputnik 2', NULL, 'Life Science,Solar Physics', '1957-002A', 3, 508.3, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2', NULL, NULL, NULL),
(3, 'Sputnik 3', NULL, 'Astronomy,Earth Science,Life Science,Space Physics', '1958-004B', 8, 1327, NULL, NULL, NULL, 0, NULL, NULL, NULL, '3', NULL, NULL, NULL),
(4, 'Sputnik 4', NULL, 'Engineering,Life Science', '1960-005A', 34, 1477, NULL, NULL, NULL, 0, NULL, NULL, NULL, '4', NULL, NULL, NULL),
(5, 'Sputnik 5', NULL, 'Engineering,Life Science', '1960-011A', 55, 4600, NULL, NULL, NULL, 0, 'Korabl Sputnik 2', NULL, NULL, '5', NULL, NULL, NULL),
(6, 'Sputnik 6', NULL, 'Engineering,Life Science', '1960-017A', 65, 4563, NULL, NULL, NULL, 0, 'Korabl Sputnik 3', NULL, NULL, '6', NULL, NULL, NULL),
(7, 'Sputnik 7', NULL, 'Engineering,Planetary Science', '1961-002A', 71, 6843, NULL, NULL, NULL, 0, 'Tyazheliy Sputnik 4', NULL, NULL, '7', NULL, NULL, NULL),
(8, 'Explorer 1', NULL, 'Earth Science,Space Physics', '1958-001A', 21, 13.97, NULL, NULL, NULL, 0, NULL, NULL, NULL, '8', NULL, NULL, NULL),
(9, 'Luna 2', NULL, 'Planetary Science,Space Physics', '1959-014A', 114, 390.2, NULL, NULL, NULL, 0, 'Lunik 2,Second Cosmic Rocket', NULL, NULL, '9', NULL, NULL, NULL),
(10, 'Luna 3', NULL, 'Planetary Science,Space Physics', '1959-008A', 21, 1959, NULL, NULL, NULL, 0, 'Lunik 3,Automatic Interplanetary Station', NULL, NULL, '10', NULL, NULL, NULL),
(11, 'TIROS 1', NULL, 'Earth Science', '1960-002B', 29, 122.5, NULL, NULL, NULL, 0, 'TIROS-A', NULL, NULL, '11', NULL, NULL, NULL),
(12, 'Vostok 1', NULL, '6', '1961-012A', 103, 4725, NULL, NULL, NULL, 0, 'Sputnik 11', NULL, NULL, '12', NULL, NULL, NULL),
(13, 'Mercury Redstone 3', NULL, 'Engineering,Human Crew', 'MERCR3', NULL, 955, NULL, NULL, NULL, 0, 'MR-3,Freedom 7', NULL, NULL, '13', NULL, NULL, NULL),
(14, 'Mariner 1', NULL, 'Planetary Science,Space Physics', 'MARIN1', NULL, 202.8, NULL, NULL, NULL, 0, NULL, NULL, NULL, '14', NULL, NULL, NULL),
(15, 'Mariner 2', NULL, 'Astronomy,Planetary Science,Space Physics', '1962-041A', 374, 202.8, NULL, NULL, NULL, 0, 'Mariner-Venus 1962', NULL, NULL, '15,16,17', NULL, NULL, NULL),
(16, 'Apollo 8', NULL, 'Human Crew,Planetary Science', '1968-118A', 3626, 28817, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Venera 1', NULL, 'Planetary Science', '1961-003A', 80, 643.5, NULL, NULL, NULL, 0, 'Venus 1,Gamma (Venera)', NULL, NULL, '18,19', NULL, NULL, NULL),
(18, 'Apollo 11 Command and Service Module (CSM)', NULL, 'Human Crew,Planetary Science', '1969-059A', 4039, 28801, NULL, NULL, NULL, 0, 'Apollo 11 CSM,Columbia,CSM-107', NULL, NULL, '21,22,23', NULL, NULL, NULL),
(19, 'Apollo 11 Lunar Module / EASEP', NULL, 'Human Crew,Planetary Science', '1969-059C', 4041, 15065, NULL, NULL, NULL, 0, 'Apollo 11 LM/EASEP,LM-5,Eagle', NULL, NULL, '25,20,24,26', NULL, NULL, NULL),
(20, 'Salyut 1', NULL, '6', '1971-032A', 5160, 18425, NULL, NULL, NULL, 0, 'Salute 1', NULL, NULL, '27,28,36,37,38', NULL, NULL, NULL),
(21, 'Soyuz 10', NULL, '6', '1971-034A', 5172, 6525, NULL, NULL, NULL, 0, '', NULL, NULL, '29,30', NULL, NULL, NULL),
(22, 'Soyuz 11', NULL, '6', '1971-053A', 5283, 6565, NULL, NULL, NULL, 0, '05283', NULL, NULL, '31,32,33,34,35', NULL, NULL, NULL),
(23, 'Apollo 17 Command and Service Module (CSM)', NULL, 'Astronomy,Human Crew,Life Science,Planetary Science', '1972-096A', 6300, 30320, NULL, NULL, NULL, 0, 'Apollo 17 CSM,CSM-114,America,Apollo 17A', NULL, NULL, '39,44,45,46,50,51', NULL, NULL, NULL),
(24, 'Apollo 17 Lunar Module /ALSEP', NULL, 'Human Crew,Planetary Science,Space Physics\r\n', '1972-096C', 6307, 16448, NULL, NULL, NULL, 0, 'Challenger,LEM 17,Rover 17,Apollo 17 LM/ALSEP,LM-12,Apollo 17C', NULL, NULL, '40,41,42,43,49,48,47', NULL, NULL, NULL),
(25, 'Skylab', NULL, 'Astronomy,Engineering,Earth Science,Human Crew,Life Science,Planetary Science,Solar Physics,Space Physics', '1973-027A', 6633, 90607, NULL, NULL, NULL, 0, 'Skylab 1', NULL, NULL, '52,57,58', NULL, NULL, NULL),
(26, 'Skylab CSM 1', NULL, 'Engineering,Human Crew,Life Science,Microgravity,Space Physics', '1973-032A', 6655, 19979, NULL, NULL, NULL, 0, 'Skylab 2', NULL, NULL, '53,54,56,55', NULL, NULL, NULL),
(27, 'Sputnik 9', NULL, 'Engineering,Life Science', '1961-008A', 91, 4700, NULL, NULL, NULL, 0, 'Korabl Sputnik 4', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'Explorer 2', NULL, 'Space Physics', 'EXPLR2', NULL, 14.52, NULL, NULL, NULL, 0, NULL, NULL, NULL, '59', NULL, NULL, NULL),
(29, 'Explorer 3', NULL, 'Astronomy,Space Physics', '1958-003A', 6, 14.1, NULL, NULL, NULL, 0, '', NULL, NULL, '60,61', NULL, NULL, NULL),
(30, 'Explorer 4', NULL, 'Space Physics', '1958-005A', 9, 25.5, NULL, NULL, NULL, 0, '', NULL, NULL, '62,63', NULL, NULL, NULL),
(31, 'Explorer 5', NULL, 'Space Physics', 'EXPLR5', NULL, 17.24, NULL, NULL, NULL, 0, NULL, NULL, NULL, '64', NULL, NULL, NULL),
(32, 'Explorer 6', NULL, 'Planetary Science,Space Physics', '1959-004A', 15, 64.4, NULL, NULL, NULL, 0, 'S-2, Able 3', NULL, NULL, '65,66,67', NULL, NULL, NULL),
(33, 'Explorer 7', NULL, 'Earth Science, Space Physics', '1959-009A', 22, 41.5, NULL, NULL, NULL, 0, 'S-1A', NULL, NULL, '68,69', NULL, NULL, NULL),
(34, 'Explorer 8', NULL, 'Earth Science,Planetary Science,Space Physics\r\n', '1960-014A', 60, 40.88, NULL, NULL, NULL, 0, 'S-30', NULL, NULL, '70,71', NULL, NULL, NULL),
(35, 'S-56a', NULL, 'Earth Science', '1961-004A', 81, 36, NULL, NULL, NULL, 0, 'Air Density 1', NULL, NULL, '72,73', NULL, NULL, NULL),
(36, 'P 14', NULL, 'Space Physics', '1961-010A', 98, 79, NULL, NULL, NULL, 0, 'Explorer 10,Explorer X', NULL, NULL, '74,75', NULL, NULL, NULL),
(37, 'S 15', NULL, 'Astronomy,Space Physics', '1961-013A', 107, 37.2, NULL, NULL, NULL, 0, 'Explorer 11', NULL, NULL, '76,77', NULL, NULL, NULL),
(38, 'EPE-A', NULL, 'Space Physics', '1961-020A', 170, 37.6, NULL, NULL, NULL, 0, 'S 3,EPE-A', NULL, NULL, '78', NULL, NULL, NULL),
(39, 'S 55A', NULL, 'Planetary Science', '1961-022A', 180, 86, NULL, NULL, NULL, 0, 'Explorer 13', NULL, NULL, '79', NULL, NULL, NULL),
(40, 'EPE-B', NULL, 'Space Physics', '1962-051A', 432, 40, NULL, NULL, NULL, 0, 'Explorer 14', NULL, NULL, '80,81', NULL, NULL, NULL),
(41, 'EPE-C', NULL, 'Space Physics', '1962-059A', 445, 44.4, NULL, NULL, NULL, 0, 'Explorer 15,S 3B', NULL, NULL, '82', NULL, NULL, NULL),
(42, 'EPE-D', NULL, 'Space Physics', '1964-086A', 963, 45.8, NULL, NULL, NULL, 0, 'Explorer 26', NULL, NULL, '83,84', NULL, NULL, NULL),
(43, 'S 55B', NULL, 'Planetary Science', '1962-070A', 506, 100.8, NULL, NULL, NULL, 0, 'Explorer 16', NULL, NULL, '85,86', NULL, NULL, NULL),
(44, 'AE-A', NULL, 'Earth Science\r\nSpace Physics', '1963-009A', 564, 183.7, NULL, NULL, NULL, 0, 'Explorer 17,S 6,Atmosphere Explorer-A', NULL, NULL, '89,87,88', NULL, NULL, NULL),
(45, 'IE-A', NULL, 'Astronomy,Space Physics', '1964-051A', 870, 44.5, NULL, NULL, NULL, 0, 'Explorer 20,S 48,TOPSI', NULL, NULL, '90,91', NULL, NULL, NULL),
(46, 'AD-A', NULL, 'Earth Science', '1963-053A', 714, 7.7, NULL, NULL, NULL, 0, 'Explorer 19', NULL, NULL, '92,93', NULL, NULL, NULL),
(47, 'IE-A', NULL, 'Astronomy,Space Physics', '1964-051A', NULL, 44.5, NULL, NULL, NULL, 0, 'Explorer 20,S 48,TOPSI,00870\r\n', NULL, NULL, '94,95', NULL, NULL, NULL),
(48, 'IMP-B', NULL, 'Space Physics', '1964-060A', NULL, 135, NULL, NULL, NULL, 0, 'IMP 2,Explorer 21,00889', NULL, NULL, '96', NULL, NULL, NULL),
(49, 'BE-B', NULL, 'Earth Science,Space Physics', '1964-064A', NULL, 52.6, NULL, NULL, NULL, 0, 'Explorer 22, Beacon Explorer-B,00899', NULL, NULL, '97', NULL, NULL, NULL),
(50, 'S 55C', NULL, 'Planetary Science', '1964-074A', NULL, 133.8, NULL, NULL, NULL, 0, 'Explorer 23,00924', NULL, NULL, '98', NULL, NULL, NULL),
(51, 'AD-B', NULL, 'Earth Science', '1964-076A', NULL, 8.6, NULL, NULL, NULL, 0, 'Explorer 24, 00931', NULL, NULL, '99', NULL, NULL, NULL),
(52, 'Injun 4', NULL, 'Space Physics', '1964-076B', NULL, 40, NULL, NULL, NULL, 0, 'Explorer 25,00932', NULL, NULL, '100', NULL, NULL, NULL),
(53, 'BE-C', NULL, 'Earth Science,Space Physics', '1965-032A', NULL, 60.8, NULL, NULL, NULL, 0, 'Explorer 27,01328', NULL, NULL, '101', NULL, NULL, NULL),
(54, 'IMP-C', NULL, 'Space Physics', '1965-042A', NULL, 128, NULL, NULL, NULL, 0, 'Explorer 28,IMP 3,01388', NULL, NULL, '102', NULL, NULL, NULL),
(55, 'GEOS 1', NULL, 'Earth Science', '1965-089A', NULL, 387, NULL, NULL, NULL, 0, 'GEOS-A,Explorer 29,01726', NULL, NULL, '103', NULL, NULL, NULL),
(56, 'SOLRAD 8', NULL, 'Solar Physics', '1965-093A', NULL, 56.7, NULL, NULL, NULL, 0, 'SE-A,Explorer 30,01738\r\n', NULL, NULL, '104,105', NULL, NULL, NULL),
(57, 'DME-A', NULL, 'Space Physics', '1965-098B', NULL, 98.9, NULL, NULL, NULL, 0, 'Explorer 31,01806', NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'AE-B', NULL, 'Earth Science,Space Physics', '1966-044A', NULL, 224.5, NULL, NULL, NULL, 0, 'Atmosphere Explorer-B, Explorer 32, 02183', NULL, NULL, '106,107', NULL, NULL, NULL),
(59, 'Explorer 33', NULL, 'Astronomy, Planetary Science, Solar Physics, Space Physics', '1966-058A\n\n', NULL, 212, NULL, NULL, NULL, 0, 'AIMP 1, Anchored IMP 1, IMP-D, 02258', NULL, NULL, '108,109', NULL, NULL, NULL),
(60, 'CSM-008', NULL, 'Suborbital test flight', NULL, NULL, 15294, NULL, NULL, NULL, 0, NULL, NULL, NULL, '110', NULL, NULL, NULL),
(61, 'CSM-011', NULL, 'Suborbital test flight', NULL, NULL, 20091, NULL, NULL, NULL, 0, 'SA-202', NULL, NULL, '111,112', NULL, NULL, NULL),
(62, 'CSM-012', NULL, 'Crewed spacecraft verification test', NULL, NULL, 20000, NULL, NULL, NULL, 0, 'AS-204', NULL, NULL, '113', NULL, NULL, NULL),
(63, 'CSM-017', NULL, 'Engineering', '1967-113A', NULL, 36856, NULL, NULL, NULL, 0, 'AS-501,3032', NULL, NULL, '114,115', NULL, NULL, NULL),
(64, 'Apollo 5', NULL, 'Engineering', '1968-007A', NULL, 14360, NULL, NULL, NULL, 0, 'AS-204, 03106', NULL, NULL, '116,117,118', NULL, NULL, NULL),
(65, 'Apollo 5 LM-1/Descent', NULL, 'Engineering', '1968-007B', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '119,120,121', NULL, NULL, NULL),
(66, 'CSM-020', NULL, 'test flight, Engineering', '1968-025A', NULL, 36930, NULL, NULL, NULL, 0, 'AS-502, 03170', NULL, NULL, '122,123', NULL, NULL, NULL),
(67, 'CSM-101', NULL, 'Manned CSM test flight, Earth Science, Human Crew\n', '1968-089A', NULL, 14781, NULL, NULL, NULL, 0, '03486', NULL, NULL, '124,125', NULL, NULL, NULL),
(68, 'CSM-104', NULL, 'Lunar Module test flight', '1969-018A', NULL, 26801, NULL, NULL, NULL, 0, '3769', NULL, NULL, '126,127,128,130,137,131,133,135', NULL, NULL, NULL),
(69, 'LM-3', NULL, 'Lunar Module test flight', '1969-018C', NULL, 14575, NULL, NULL, NULL, 0, '3771', NULL, NULL, '126,129,130,136,132,134,135', NULL, NULL, NULL),
(70, 'CSM-106', NULL, 'Human Crew,Planetary Science', '1969-043A', NULL, 28870, NULL, NULL, NULL, 0, '3941,Charlie Brown', NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'LM-4', NULL, 'Human Crew,Planetary Science', '1969-043C', NULL, 13941, NULL, NULL, NULL, 0, '3948,Snoopy', NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'CSM-108', NULL, 'Human Crew,Planetary Science', '1969-099A', NULL, 28790, NULL, NULL, NULL, 0, 'Yankee Clipper, Apollo 12 CSM, 04225', NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'LM-6', NULL, 'Human Crew,Planetary Science', '1969-099C', NULL, 15235, NULL, NULL, NULL, 0, 'Apollo 12 LM/ALSEP, LEM 12, Intrepid, Apollo 12C, 04246', NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'CSM-109', NULL, 'Human Crew,Planetary Science', '1970-029A\n\n', NULL, 28945, NULL, NULL, NULL, 0, 'Odyssey, Apollo 13 CSM, CSM-109, 04371', NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'LM-7', NULL, 'Human Crew,Planetary Science', '1970-029C\n\n', NULL, 15196, NULL, NULL, NULL, 0, 'Aquarius, LM-7, ALSEP 13, LEM 13', NULL, NULL, NULL, NULL, NULL, NULL),
(76, 'CSM-110', NULL, 'Human Crew,Planetary Science', '1971-008A', NULL, 29229, NULL, NULL, NULL, 0, 'Kitty Hawk, Apollo 14 CSM, 04900', NULL, NULL, NULL, NULL, NULL, NULL),
(77, 'LM-8', NULL, 'Human Crew,Planetary Science', '1971-008C', NULL, 15264, NULL, NULL, NULL, 0, 'LM-8, Antares, Apollo 14 LM/ALSEP, LEM 14, Apollo 14C, 04905\n', NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'CSM-112', NULL, 'Human Crew,Planetary Science', '1971-063A', NULL, 30371, NULL, NULL, NULL, 0, 'CSM-112, Endeavor, Apollo 15 CSM, 05351', NULL, NULL, NULL, NULL, NULL, NULL),
(79, 'LM-10', NULL, 'Human Crew,Planetary Science', '1971-063C', NULL, 16430, NULL, NULL, NULL, 0, 'Falcon, LM-10, Apollo 15C, Apollo 15 LM/ALSEP, LEM 15, Rover 15, 05366', NULL, NULL, NULL, NULL, NULL, NULL),
(80, 'CSM-113', NULL, 'Human Crew,Planetary Science', '1972-031A', NULL, 30354, NULL, NULL, NULL, 0, 'Apollo 16 CSM, Casper, CSM-113, 06000', NULL, NULL, NULL, NULL, NULL, NULL),
(81, 'LM-11', NULL, 'Human Crew,Planetary Science', '1972-031C', NULL, 16445, NULL, NULL, NULL, 0, 'Apollo 16 LM/ALSEP, LEM 16, Rover 16, Apollo 16C, Orion, LM-11, ALSEP 16, 06005', NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'Vanguard TV-0', NULL, 'test flight', 'none', NULL, NULL, NULL, NULL, NULL, 0, 'Vanguard Test Vehicle Zero', NULL, NULL, '138', NULL, NULL, NULL),
(83, 'Vanguard TV-1', NULL, 'test flight', 'none', NULL, NULL, NULL, NULL, NULL, 0, 'Vanguard Test Vehicle One', NULL, NULL, '139', NULL, NULL, NULL),
(84, 'Vanguard TV-2', NULL, 'test flight', 'none', NULL, NULL, NULL, NULL, NULL, 0, 'Vanguard Test Vehicle Two', NULL, NULL, '140', NULL, NULL, NULL),
(85, 'QZS 2', NULL, 'Navigation/Global Positioning', '2017-028A', NULL, 4000, NULL, NULL, NULL, 0, 'Michibiki 2, 42738', 'JAXA', 'Mitsubishi Electric (MELCO)', '141', NULL, NULL, NULL),
(86, 'QZS 1', NULL, 'Navigation/Global Positioning', '2010-045A', NULL, 4100, NULL, NULL, NULL, 0, 'Michibiki 1, 37158', 'JAXA', 'Mitsubishi Electric (MELCO)', '142', NULL, NULL, NULL),
(87, 'Zarja', NULL, 'Engineering, Human Crew', '1998-067A', NULL, 19323, NULL, NULL, NULL, 0, 'ISS, MKS, Space Station, Zarya, Unity, FGB, 25544', NULL, NULL, '143,145,152,153,156,157', NULL, NULL, NULL),
(88, 'Zvezda', NULL, 'Service module', NULL, NULL, 20320, NULL, NULL, NULL, 0, 'DOS-8', NULL, NULL, '144,147,149,180,181', NULL, NULL, NULL),
(89, 'Progress M1-3 11F615A55', NULL, 'ISS resupply', '2000-044A', NULL, NULL, NULL, NULL, NULL, 0, '26461', 'Roskosmos', NULL, '146,148,158', NULL, NULL, NULL),
(90, 'Progress M1-4 11F615A55', NULL, 'ISS resupply', '2000-073A', NULL, NULL, NULL, NULL, NULL, 0, '26615', NULL, NULL, '150,151,154,155,159', NULL, NULL, NULL),
(91, 'QZS 3', NULL, 'Navigation/Global Positioning', '2017-048A', NULL, 4700, NULL, NULL, NULL, 0, 'Michibiki 3, 42917', 'JAXA', 'Mitsubishi Electric (MELCO)', '168', NULL, NULL, NULL),
(92, 'Vanguard 1', NULL, 'Engineering, Earth Science, Space Physics', '1958-002B', NULL, 1.5, NULL, NULL, NULL, 0, 'Vanguard TV-4, 00005', 'NASA', 'Naval Research Laboratory (NRL)', '160,161,162', NULL, NULL, NULL),
(93, 'OPTSAT-3000', NULL, 'Surveillance and Other Military, Other', '2017-044A', NULL, 368, NULL, NULL, NULL, 0, '42900, SHALOM', 'Italian Defense Ministry', 'IAI', '163', NULL, NULL, NULL),
(94, 'VENµS', NULL, 'Earth Science', '2017-044B', NULL, 264, NULL, NULL, NULL, 0, '42901, Venus (Vegetation and Environment Monitoring New Micro-Satellite)', 'IAI, CNES', 'IAI, Rafael', '164', NULL, NULL, NULL),
(95, 'Dragon CRS 12', NULL, 'Resupply/Refurbishment/Repair', '2017-045A', NULL, 6650, NULL, NULL, NULL, 0, 'SpX CRS-12, CRS 12, 42904', 'SpaceX', 'SpaceX', '165', NULL, NULL, NULL),
(96, 'CREAM', NULL, 'Cosmic radiation science', NULL, NULL, 1300, NULL, NULL, NULL, 0, 'Cosmic Ray Energetics And Mass, ISS-CREAM', 'NASA Goddard Space Flight Center\r\n', 'NASA Goddard Space Flight Center', NULL, NULL, NULL, NULL),
(97, 'Kestrel Eye 2M', NULL, 'Reconnaissance, electro-optical', NULL, NULL, 50, NULL, NULL, NULL, 0, 'Kestrel Eye Block 2M', 'US Army', 'Maryland Aerospace, Inc. (ex IntelliTech Microsystems, Inc.)', NULL, NULL, NULL, NULL),
(98, 'Dellingr', NULL, 'Technology, heliophysics, magnetospheric research', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'RBLE, Radiation Belt Loss Experiment', 'NASA Goddard Space Flight Center, SwRI', 'NASA Goddard Space Flight Center', NULL, NULL, NULL, NULL),
(99, 'ASTERIA', NULL, 'Technology', NULL, NULL, 12, NULL, NULL, NULL, 0, 'Arcsecond Space Telescope Enabling Research in Astrophysics', 'Massachusetts Institute of Technology - Space Systems Laboratory (MIT SSL), Jet Propulsion Laboratory (JPL)', 'Massachusetts Institute of Technology - Space Systems Laboratory (MIT SSL), Jet Propulsion Laboratory (JPL)', NULL, NULL, NULL, NULL),
(100, 'OSIRIS-3U', NULL, 'Life sciences', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Orbital Satellite for Investigating the Response of the Ionosphere to Stimulation and Space Weather', 'The Pennsylvania State University Department of Electrical Engineering', 'The Pennsylvania State University Department of Electrical Engineering', NULL, NULL, NULL, NULL),
(101, 'Kosmos 2520', NULL, 'Communication', '2017-046A', NULL, NULL, NULL, NULL, NULL, 0, 'Blagovest 11L, 42907', NULL, 'ISS Reshetnev (prime); Thales Alenia Space (payload)', '175', NULL, NULL, NULL),
(102, 'TDRS M', NULL, 'Communication, data relay', '2017-047A', NULL, 3454, NULL, NULL, NULL, 0, '42915', 'NASA', 'Boeing', '166', NULL, NULL, NULL),
(103, 'FORMOSAT 5', NULL, 'Earth observing', '2017-049A', NULL, 475, NULL, NULL, NULL, 0, NULL, 'NSPO', NULL, '167', NULL, NULL, NULL),
(104, 'Enhanced Cygnus', NULL, 'ISS resupply', '2017-019A', NULL, 3459, NULL, NULL, NULL, 0, '42681', 'NASA', 'United Launch Alliance', '169,170,171,172', NULL, NULL, NULL),
(105, 'Unity', NULL, '', '1998-069F', NULL, 11612, NULL, NULL, NULL, 0, '25575', 'National Aeronautics and Space Administration (United States)', NULL, '173', NULL, NULL, NULL),
(106, 'SES-10', NULL, 'Communications', '2017-017A', NULL, 5281.7, NULL, NULL, NULL, 0, 'S2950', 'SES', 'SpaceX', '174', NULL, NULL, NULL),
(107, 'WGS 9', NULL, 'Communications', '2017-016A', NULL, 5987, NULL, NULL, NULL, 0, 'USA 275, 42075', 'USAF', 'Boeing', '176', NULL, NULL, NULL),
(108, 'TDRS 13', NULL, 'Communication, data relay', '2017-047A', NULL, 3454, NULL, NULL, NULL, 0, 'TDRS M, 42915', 'NASA', 'Boeing', '177', NULL, NULL, NULL),
(109, 'Progress M-44', NULL, 'ISS resupply', '2001-008A', NULL, 7250, NULL, NULL, NULL, 0, 'Progress 3, 3P, 26713', 'RKK → RAKA', NULL, '178,179,182,183', NULL, NULL, NULL),
(110, 'Pirs', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 'Progress M-SO1', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 'Progress M1-7', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 'Progress M1-8', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 'Progress M-46', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 'Progress M1-9', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 'Progress M-47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 'Progress M1-9', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 'Progress M-47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 'Progress M1-10', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 'Progress M-48', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 'Progress M1-11', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 'Progress M-49', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 'Progress M-50', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 'Progress M-51', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 'Progress M-52', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 'Progress M-53', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 'Progress M-54', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 'Progress M-55', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 'Progress M-56', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 'Progress M1-6', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 'Progress M-45', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 'Progress M-57', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 'Progress M-58', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 'Progress M-59', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 'Progress M-60', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 'Progress M-61', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 'Progress M-62', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 'Progress M-63', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `PossibleOutcome`
--

CREATE TABLE `PossibleOutcome` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PossibleOutcome`
--

INSERT INTO `PossibleOutcome` (`ID`, `Name`) VALUES
(1, 'Successful');

-- --------------------------------------------------------

--
-- Table structure for table `Post`
--

CREATE TABLE `Post` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `AstronautIDs` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Post`
--

INSERT INTO `Post` (`ID`, `Name`, `AstronautIDs`) VALUES
(1, 'Commander', '9'),
(2, 'Command Module Pilot', '10'),
(3, 'Lunar Module Pilot', '11');

-- --------------------------------------------------------

--
-- Table structure for table `Power`
--

CREATE TABLE `Power` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Power`
--

INSERT INTO `Power` (`ID`, `Name`) VALUES
(1, 'Batteries');

-- --------------------------------------------------------

--
-- Table structure for table `Propulsion`
--

CREATE TABLE `Propulsion` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) COLLATE utf8_bin NOT NULL,
  `CountyOfOrigin` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `CombustionChamberPressure` int(11) DEFAULT NULL COMMENT 'in PSI',
  `ThrustAtSeaLevel` float DEFAULT NULL COMMENT 'in kN',
  `ThrustVacuum` float DEFAULT NULL COMMENT 'in kN',
  `SpecificImpulseAtSeaLevel` float DEFAULT NULL COMMENT 'in m/s',
  `SpecificImpulseVacuum` float DEFAULT NULL COMMENT 'in m/s',
  `Height` float DEFAULT NULL COMMENT 'in cm',
  `Diameter` float DEFAULT NULL COMMENT 'in cm',
  `Cycle` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Thrust-to-weightRatio` float DEFAULT NULL,
  `Designer` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Propulsion`
--

INSERT INTO `Propulsion` (`ID`, `Name`, `CountyOfOrigin`, `CombustionChamberPressure`, `ThrustAtSeaLevel`, `ThrustVacuum`, `SpecificImpulseAtSeaLevel`, `SpecificImpulseVacuum`, `Height`, `Diameter`, `Cycle`, `Thrust-to-weightRatio`, `Designer`, `Status`) VALUES
(1, 'RD-107 (AKA 8D74)', NULL, 853, 813.98, 1000.31, 2510, 3070, 28.65, 18.5, 'Gas generator', NULL, NULL, NULL),
(2, 'RD-108 (AKA 8D75)', NULL, 740, 745.33, 941.47, 2430, 3090, 286.5, 195, 'Gas generator', 74.4, NULL, NULL),
(3, 'RD-107K (AKA 8D74K)', NULL, 853, 818.88, 995.41, 2512, 3072, 286.5, 185, 'Gas generator', NULL, NULL, NULL),
(4, 'RD-107 (AKA 8D728 or 8D74M)', NULL, 848, 755.14, 921.86, 2520, 3080, 286.5, 185, 'Gas generator', NULL, NULL, NULL),
(5, 'RD-117 (AKA 11D511)', NULL, 772, 778.68, 0, 2480, 3100, 286.5, 185, 'Gas generator', NULL, NULL, NULL),
(6, 'RD-107A (AKA 14D22)', NULL, 870, 839.48, 1019.93, 2582, 3140, 257.8, 185, 'Gas generator', NULL, NULL, NULL),
(7, 'RD-107A (AKA 14D22KhZ)', NULL, 870, 839.48, 1019.93, 2582, 3140, 257.8, 185, 'Gas generator', NULL, NULL, NULL),
(8, 'H-1', NULL, 700, 7100, NULL, 2500, 2830, 268.224, 149.352, 'Gas generator', 102.47, NULL, NULL),
(9, 'J-2', NULL, 763, 486.2, 1033.1, 2000, 4130, 3.4, 2.1, 'Gas generator', 73.18, NULL, NULL),
(10, 'Rocketdyne A-7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Baby-Sergant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Merlin 1A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Gas generator', NULL, NULL, NULL),
(13, 'Merlin 1B', NULL, NULL, 380, 420, NULL, NULL, NULL, NULL, 'Gas generator', NULL, NULL, NULL),
(14, 'Merlin 1C', NULL, 982, 420, 480, NULL, NULL, NULL, NULL, 'Gas generator', NULL, NULL, NULL),
(15, 'Merlin Vacuum (1C)', NULL, 982, 420, 480, 2600, 3000, NULL, NULL, 'Gas generator', NULL, NULL, NULL),
(16, 'Merlin 1D', NULL, 1410, 845, 914, 2770, 3050, NULL, NULL, 'Gas generator', 180.1, NULL, NULL),
(17, 'Merlin 1D Vacuum', NULL, NULL, NULL, 934, NULL, NULL, NULL, NULL, 'Gas generator', NULL, NULL, NULL),
(18, 'None', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `SLSfamily`
--

CREATE TABLE `SLSfamily` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `SLSstages`
--

CREATE TABLE `SLSstages` (
  `ID` int(11) NOT NULL,
  `Type` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `DryMass` float DEFAULT NULL COMMENT 'in tone',
  `GrossMass` float DEFAULT NULL COMMENT 'in tone',
  `BurnTime` time DEFAULT NULL,
  `Diameter` float DEFAULT NULL COMMENT 'in meter',
  `Length` float DEFAULT NULL COMMENT 'in meter',
  `Propellants` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `EngineIDs` varchar(256) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `SLSstages`
--

INSERT INTO `SLSstages` (`ID`, `Type`, `DryMass`, `GrossMass`, `BurnTime`, `Diameter`, `Length`, `Propellants`, `EngineIDs`) VALUES
(1, 'Strap-on boosters', 3.4, 43, '00:02:00', 2.7, 19, 'LOX/RP-1', '1'),
(2, NULL, 7.4, 94, '00:05:40', 3, 28, 'LOX/RP-1', '2'),
(3, NULL, 3.4, 43, '00:02:12', 2.7, 19, 'LOX/RP-1', NULL),
(4, NULL, 7.1, 95, '00:06:30', 2.99, 28, 'LOX/RP-1', NULL),
(5, 'S-IB', 42, 399.4, '00:02:30', 6.53, 24.5, 'RP-1/LOX', '8x8'),
(6, 'S-IVB', 11, 115, '00:07:55', 6.6, 17.81, 'LOX/LH2', '9'),
(7, NULL, 6.16, 30.96, '00:02:35', NULL, NULL, 'LOX/Hydyne', '10'),
(8, NULL, 0.09, 0.327, '00:00:01', 0.86, NULL, 'TPH', '11x11'),
(9, NULL, 0.028, 0.094, '00:00:06', 0.41, NULL, 'TPH', '3x11'),
(10, NULL, 0.005, 0.027, '00:00:06', 0.15, NULL, 'TPH', '1x11'),
(11, NULL, NULL, NULL, '00:02:50', NULL, NULL, 'LOX/RP-1', '9x14'),
(12, NULL, NULL, NULL, '00:05:45', NULL, NULL, 'LOX/RP-1', '15'),
(13, NULL, NULL, NULL, '00:03:00', NULL, NULL, 'LOX/RP-1', '9x16'),
(14, NULL, NULL, NULL, '00:06:15', NULL, NULL, 'LOX/RP-1', '17'),
(15, NULL, 22200, 433100, '00:02:42', 3.66, 42.6, 'Subcooled LOX / Chilled RP-1', '9x16'),
(16, NULL, 4000, 111500, '00:06:37', 3.66, 12.6, 'LOX/RP-1', '17');

-- --------------------------------------------------------

--
-- Table structure for table `SpaceLaunchSystem`
--

CREATE TABLE `SpaceLaunchSystem` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) COLLATE utf8_bin NOT NULL,
  `FamilyID` int(11) DEFAULT NULL,
  `MaxPayloadLEO` int(11) DEFAULT NULL COMMENT 'in kg',
  `MaxHeight` float DEFAULT NULL COMMENT 'in m',
  `MaxDiameter` float DEFAULT NULL COMMENT 'in m',
  `Manufacturer` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `StageIDs` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Nation` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `MissionEvent` varchar(256) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `SpaceLaunchSystem`
--

INSERT INTO `SpaceLaunchSystem` (`ID`, `Name`, `FamilyID`, `MaxPayloadLEO`, `MaxHeight`, `MaxDiameter`, `Manufacturer`, `StageIDs`, `Nation`, `MissionEvent`) VALUES
(1, 'Sputnik (8K71PS)', NULL, 500, 30, 2.99, 'OKB-1', '4x1,2', NULL, NULL),
(2, 'Sputnik (8A91)', NULL, 1327, 31, 2.99, 'OKB-1', '4x3,4', NULL, NULL),
(3, 'Modified SS-6 (Sapwood) with 1st Generation Upper Stage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Modified SS-6 (Sapwood) with 2nd Generation Upper Stage + Escape Stage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Juno I', NULL, NULL, 21.2, 1.78, NULL, '7,8,9,10', NULL, NULL),
(6, 'Modified SS-6 (Sapwood) with 2nd Generation (Longer) Upper Stage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Thor (PGM-17 Thor)', NULL, NULL, 20, 2.44, NULL, NULL, NULL, NULL),
(8, 'Redstone Mercury', NULL, NULL, 26, 1.78, NULL, NULL, NULL, NULL),
(9, 'Atlas LV-3 Agena-B', NULL, 1000, 36, 3, NULL, NULL, NULL, NULL),
(10, 'Saturn V', NULL, 133000, 110.6, 10.6, NULL, NULL, NULL, NULL),
(11, 'Proton-K', NULL, 19760, 49, 7.4, NULL, NULL, NULL, NULL),
(12, 'Saturn IB', NULL, 21000, 68, 6.61, NULL, '5,6', NULL, NULL),
(13, 'Thor Able III', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Juno II', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Scout ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Thor Delta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Scout-X1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Delta-I A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Delta-I B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'Delta-C1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'Delta-E1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Falcon 9 v1.0', NULL, 10450, 47.8, 3.66, NULL, '11,12', NULL, NULL),
(23, 'Falcon 9 v1.1', NULL, 13150, 68.4, 3.66, NULL, '13,14', NULL, NULL),
(24, 'Falcon 9 Full Thrust', NULL, 22800, 70, 3.66, NULL, '15,16', NULL, NULL),
(25, 'Viking', NULL, 9, 23, 1.14, 'Glenn L. Martin Company', NULL, NULL, NULL),
(26, 'Vanguard', NULL, 9, 23, 1.14, 'Glenn L. Martin Company', NULL, NULL, NULL),
(27, 'H-2A-202', NULL, 10000, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'Soyuz-U', NULL, NULL, NULL, NULL, 'TsSKB-Progress', NULL, NULL, NULL),
(29, 'H-2A-204', NULL, 15000, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'Vega', NULL, 2300, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'Proton-M Briz-M (8K82KM 14S43) (Phase IV)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'Atlas-5(401)', NULL, 9797, 58.3, 3.81, 'United Launch Alliance', NULL, NULL, NULL),
(33, 'Soyuz-FG Fregat (11A511U-FG)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Spaceman`
--

CREATE TABLE `Spaceman` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) COLLATE utf8_bin NOT NULL,
  `Birth` date NOT NULL,
  `Death` date DEFAULT NULL,
  `Nationality` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `TotalDurationOfSpaceflights` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `EVAs` int(11) DEFAULT NULL,
  `EVATotalDuration` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Training` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Job` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Spaceman`
--

INSERT INTO `Spaceman` (`ID`, `Name`, `Birth`, `Death`, `Nationality`, `TotalDurationOfSpaceflights`, `EVAs`, `EVATotalDuration`, `Training`, `Job`, `Status`) VALUES
(1, 'Gerst, Alexander', '1976-05-03', NULL, '4', '165:08:01', 1, '06:13:00', 'Karlsruher Institut of Technologie', 'Geophysicist', 0),
(2, 'Laika', '1954-00-00', '1957-11-03', '1', NULL, 0, NULL, NULL, NULL, 0),
(3, 'Juri Alexejewitsch Gagarin', '1934-03-09', '1968-03-27', '1', '01:48:00', 0, NULL, NULL, NULL, 0),
(4, 'Alan Bartlett Al Shepard, Jr.', '1923-11-18', '1998-07-21', '2', '09:00:16', 2, '9:23:00', NULL, NULL, 0),
(5, 'John Herschel Glenn Jr.', '1921-07-18', '2016-12-08', '2', '09:02:39', NULL, NULL, NULL, NULL, 0),
(6, 'William Alison Bill Anders', '1933-09-17', NULL, '2', '06:03:00', 0, NULL, NULL, NULL, 0),
(7, 'James Arthur Jim Lovell, Jr. ', '1928-03-25', NULL, '2', '29:19:03', NULL, NULL, NULL, NULL, 0),
(8, 'Frank Frederick Borman, II ', '1928-03-14', NULL, '2', '19:21:35', NULL, NULL, NULL, NULL, 0),
(9, 'Neil Alden Armstrong', '1930-08-05', '2012-08-25', '2', NULL, 1, '2h 31min', NULL, NULL, 0),
(10, 'Michael Collins', '1930-10-31', NULL, '2', NULL, 2, '38 min', NULL, NULL, 0),
(11, 'Buzz Aldrin', '1930-01-20', NULL, '2', NULL, 4, '4h 37min', NULL, NULL, 0),
(12, 'Vladimir Aleksandrovich Shatalov', '1972-12-08', NULL, '1', '09:21:55', NULL, NULL, 'Cosmonaut', 'Pilot', 0),
(13, 'Aleksei Stanislavovich Yeliseyev', '1934-07-13', NULL, '1', '08:22:20', 1, '37min', NULL, 'Engineer', 0),
(14, 'Nikolay Nikolayevich Rukavishnikov', '1932-09-18', '2002-10-19', '1', '09:21:09', NULL, NULL, NULL, 'Physicist', 0),
(15, 'Georgiy Timofeyevich Dobrovolsky', '1928-06-01', '1971-06-30', '1', '23:18:21', NULL, NULL, NULL, 'Pilot', 0),
(16, 'Vladislav Nikolayevich Volkov', '1935-11-23', '1971-06-30', '1', '28:17:01', NULL, NULL, NULL, 'Engineer', 0),
(17, 'Viktor Ivanovich Patsayev', '1933-06-19', '1971-06-30', '1', '23:18:21', NULL, NULL, NULL, 'Engineer', 0),
(18, 'Harrison Hagan Jack Schmitt', '1935-07-03', NULL, '2', '12:13:52', 3, '22h 4min', NULL, NULL, 0),
(19, 'Eugene Andrew Gene Cernan', '1934-03-14', '2017-01-16', '2', '23:14:15', 4, '24h 11min', NULL, NULL, 0),
(20, 'Ronald Ellwin Ron Evans, Jr.', '1933-11-10', '1990-04-07', '2', '12:13:51', 1, '1h 6min', NULL, NULL, 0),
(21, 'Joseph Peter Kerwin', '1932-02-19', NULL, '2', '28:00:50', 1, '3h 25min', NULL, 'Scientist', 0),
(22, 'Paul Joseph Weitz', '1932-07-25', NULL, '2', '33:01:13', 2, '2h 14min', NULL, NULL, 0),
(23, 'Charles Pete Conrad, Jr.', '1930-06-02', '1999-07-08', '2', '49:03:38', 4, '12h 44min', NULL, NULL, 0),
(24, 'Virgil Ivan \"Gus\" Grissom', '1926-04-03', '1967-01-27', '2', NULL, NULL, NULL, NULL, NULL, 0),
(25, 'Edward Higgins White II', '1930-11-14', '1967-01-27', '2', NULL, NULL, NULL, NULL, NULL, 0),
(26, 'Roger Bruce Chaffee', '1935-02-15', '1967-01-27', '2', NULL, NULL, NULL, NULL, NULL, 0),
(27, 'Walter Marty \"Wally\" Schirra Jr.', '1923-03-12', '2007-05-03', '2', NULL, NULL, NULL, NULL, NULL, 0),
(28, 'Donn Fulton Eisele', '1930-05-23', '1987-12-02', '2', NULL, NULL, NULL, NULL, NULL, 0),
(29, 'Ronnie Walter Cunningham', '1932-03-16', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(30, 'James Alton \"Jim\" McDivitt', '1929-06-10', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(31, 'David Randolph \"Dave\" Scott', '1932-06-06', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(32, 'Russell Louis \"Rusty\" Schweickart', '1935-10-25', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(33, 'Thomas Patten \"Tom\" Stafford', '1930-09-17', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(34, 'John Watts Young', '1930-09-24', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(35, 'Eugene Andrew \"Gene\" Cernan', '1934-03-14', '2017-01-16', '2', NULL, NULL, NULL, NULL, NULL, 0),
(36, 'John Leonard \"Jack\" Swigert Jr.', '1931-08-30', '1982-12-27', '2', NULL, NULL, NULL, NULL, NULL, 0),
(37, 'Fred Wallace Haise Jr.', '1933-11-14', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(38, 'Alan Bartlett Shepard Jr.', '1923-11-18', '1998-07-21', '2', NULL, NULL, NULL, NULL, NULL, 0),
(39, 'Stuart Allen \"Stu\" Roosa', '1933-08-13', '1994-12-12', '2', NULL, NULL, NULL, NULL, NULL, 0),
(40, 'Edgar Dean \"Ed\" Mitchell', '1930-11-17', '2016-02-04', '2', NULL, NULL, NULL, NULL, NULL, 0),
(41, 'Alfred Merrill \"Al\" Worden', '1932-02-07', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(42, 'James Benson \"Jim\" Irwin', '1930-03-17', '1991-08-08', '2', NULL, NULL, NULL, NULL, NULL, 0),
(43, 'Thomas Kenneth Mattingly II', '1936-03-17', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0),
(44, 'Charles Moss \"Charlie\" Duke Jr.', '1935-10-03', NULL, '2', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Status`
--

CREATE TABLE `Status` (
  `ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Status`
--

INSERT INTO `Status` (`ID`, `Name`) VALUES
(1, 'Retired'),
(2, 'Burnt up in Atmosphere');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Applications`
--
ALTER TABLE `Applications`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Configuration`
--
ALTER TABLE `Configuration`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Crew`
--
ALTER TABLE `Crew`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Equipment`
--
ALTER TABLE `Equipment`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `LaunchSites`
--
ALTER TABLE `LaunchSites`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `MasterMissions`
--
ALTER TABLE `MasterMissions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `MissionEvents`
--
ALTER TABLE `MissionEvents`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Missions`
--
ALTER TABLE `Missions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Nation`
--
ALTER TABLE `Nation`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Payloads`
--
ALTER TABLE `Payloads`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `PossibleOutcome`
--
ALTER TABLE `PossibleOutcome`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Post`
--
ALTER TABLE `Post`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Power`
--
ALTER TABLE `Power`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Propulsion`
--
ALTER TABLE `Propulsion`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `SLSfamily`
--
ALTER TABLE `SLSfamily`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `SLSstages`
--
ALTER TABLE `SLSstages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `SpaceLaunchSystem`
--
ALTER TABLE `SpaceLaunchSystem`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Spaceman`
--
ALTER TABLE `Spaceman`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Status`
--
ALTER TABLE `Status`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Applications`
--
ALTER TABLE `Applications`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Configuration`
--
ALTER TABLE `Configuration`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Crew`
--
ALTER TABLE `Crew`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Equipment`
--
ALTER TABLE `Equipment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `LaunchSites`
--
ALTER TABLE `LaunchSites`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `MasterMissions`
--
ALTER TABLE `MasterMissions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `MissionEvents`
--
ALTER TABLE `MissionEvents`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `Missions`
--
ALTER TABLE `Missions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `Nation`
--
ALTER TABLE `Nation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Payloads`
--
ALTER TABLE `Payloads`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `PossibleOutcome`
--
ALTER TABLE `PossibleOutcome`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Post`
--
ALTER TABLE `Post`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Power`
--
ALTER TABLE `Power`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Propulsion`
--
ALTER TABLE `Propulsion`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `SLSfamily`
--
ALTER TABLE `SLSfamily`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `SLSstages`
--
ALTER TABLE `SLSstages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `SpaceLaunchSystem`
--
ALTER TABLE `SpaceLaunchSystem`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `Spaceman`
--
ALTER TABLE `Spaceman`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `Status`
--
ALTER TABLE `Status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
