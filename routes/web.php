<?php
    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | This file is where you may define all of the routes that are handled
    | by your application. Just tell Framy the URIs it should respond
    | to using a Closure or controller method. Build something great!
    |
    */

    use \app\framework\Component\Route\Klein\Klein;

    $klein = new Klein();

    $klein->respond("GET", "/", function(){
        return view("blank");
    });

    $klein->respond("GET", "/changes", function(){
        view("blank", [
            "pageName" => "Changes",
            "text" => "Changes",
            "breadcrumb" => "fetch:core/breadcrumb_single",
            "pageContent" => "fetch:changes/changes.tpl"
        ]);
    });

    $klein->respond("GET", "/about", function (){
        view("about", [
            "pageName" => "About",
            "text" => "About",
            "breadcrumb" => "fetch:core/breadcrumb_single",
            "pageContent" => "fetch:about.tpl"
        ]);
    });

    $klein->respond("GET", "/login", function (){
        view("core/login");
    });

    $klein->respond("POST", "/logMeIn", function (){
        app("Login@logMeIn");
    });

    $klein->with("/sfdb", function () use ($klein) {

        $klein->with("/missions", function () use ($klein) {

            $klein->respond('GET', '/?', function () {
                app("SpaceFlightDatabase@missions");
            });

            $klein->respond("GET", "/master", function () {
                app("SpaceFlightDatabase@masterMissions");
            });

            $klein->respond("GET", "/master/[i:ID]", function ($request) {
                app("SpaceFlightDatabase@masterMissions", [$request->ID]);
            });

        });

        $klein->with("/mission", function () use ($klein){

            $klein->respond("GET", "/?", function () {
                app("SpaceFlightDatabase@mission");
            });

            $klein->respond("GET", "/[i:ID]", function ($request) {
                app("SpaceFlightDatabase@mission", [$request->ID]);
            });

            $klein->respond("GET", "/[i:ID]/edit", function ($request) {
                view("blank", [
                    "pageContent" => "fetch:SpaceFlightDatabase/editMission"
                ]);
            });

        });

        $klein->with("/payload", function() use ($klein) {

            $klein->respond("GET", "/[i:ID]", function ($request) {
                app("SpaceFlightDatabase@payload", [$request->ID]);
            });

        });

        $klein->with("/spaceman", function () use ($klein) {

            $klein->respond("GET",  "/[i:ID]", function ($request) {
                app("SpaceFlightDatabase@showSpacemanDetailed", [$request->ID]);
            });

        });

        $klein->with("/hangar", function () use ($klein) {

            $klein->respond("GET", "/?", function () {
                app("SpaceFlightDatabase@hangar");
            });

            $klein->respond("GET", "/[i:ID]", function ($request) {
                app("SpaceFlightDatabase@spaceLaunchSystem", [$request->ID]);
            });

        });

        $klein->with("/sorted", function () use ($klein) {

            $klein->respond('GET', '/?', function () {
                view('SpaceFlightDatabase/sorted/sorted', [
                    'breadcrumb' => view("fetch:core/breadcrumb_single", [
                            "isNotEndLevel" => true,
                            "linkToLowerLevel" => "/sfdb",
                            "text" => "Space Flight DB",
                        ]).view("fetch:core/breadcrumb_single", [
                            "text" => "Sorted",
                        ])
                ]);
            });

            $klein->respond("GET", "/byYear", function () {
                app('SpaceFlightDatabase@sorted', ['year']);
            });

            $klein->respond("GET", "/byYear/[i:ID]", function ($request) {
                app('SpaceFlightDatabase@sorted', ['year', $request->ID]);
            });

        });

        $klein->respond("/?", function() {

            view("SpaceFlightDatabase/main", [
                "text" => "Space Flight Database",
                "breadcrumb" => "fetch:core/breadcrumb_single",
                "numOfPayloads" => \app\custom\Models\SpaceFlightDB\Payloads::getInstance()->numOfDataSets(),
                "numOfMissions" => \app\custom\Models\SpaceFlightDB\Missions::getInstance()->numOfDataSets(),
                "numOfSpaceman" => \app\custom\Models\SpaceFlightDB\Spaceman::getInstance()->numOfDataSets(),
            ]);
        });

    });

    $klein->onHttpError(function ($code, $router) {
        switch ($code) {
            case 404:
                $router->response()->body(
                    view("core/error/404")
                );
                break;
            case 405:
                $router->response()->body(
                    'You can\'t do that!'
                );
                break;
            default:
                $router->response()->body(
                    'Oh no, a bad error happened that caused a '. $code
                );
        }
    });

    $klein->dispatch();